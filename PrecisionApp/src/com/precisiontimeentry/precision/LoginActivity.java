package com.precisiontimeentry.precision;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;

import com.precisiontimeentry.precision.connection.NetworkUtil;
import com.precisiontimeentry.precision.model.LoginData;
import com.precisiontimeentry.precision.model.ObscuredSharedPreferences;
import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity implements OnClickListener {

	private EditText username, password;
	private Button login, forgotPass, createAccount, demoLogin;
	private ProgressDialog progressDialog;
	private LoginActivity context;
	private boolean isDemo;
	private ObscuredSharedPreferences preferences;
	private ObscuredSharedPreferences.Editor editor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		isDemo = false;
		
		Intent i = getIntent();
		boolean invalidSession = i.getBooleanExtra("invalid_session", false);
		if(invalidSession) {
			new AlertDialog.Builder(LoginActivity.this)
			.setTitle("Session Timeout")
			.setMessage("You have had more than 15 minutes of inactivity. To protect your account, you have been logged out. Please log in again.")
			.setIcon(android.R.drawable.ic_dialog_alert)
			 .setNegativeButton(android.R.string.ok, null).show();
		}
		
		context = this;
		Initialization();
	}

	private void Initialization() {

		username = (EditText) findViewById(R.id.txt_username);
		password = (EditText) findViewById(R.id.txt_password);
		login = (Button) findViewById(R.id.btn_login);
		forgotPass = (Button) findViewById(R.id.btn_to_forgot_password);
		createAccount = (Button) findViewById(R.id.btn_to_create_account);
		demoLogin = (Button) findViewById(R.id.demoLogin);
		preferences = new ObscuredSharedPreferences(this, this.getSharedPreferences("Precision Preferences", Context.MODE_PRIVATE)); 
		editor = preferences.edit();
		if(preferences.getString("u_n", null) != null) {
			username.setText(preferences.getString("u_n", ""));
		}
		login.setOnClickListener(this);
		forgotPass.setOnClickListener(this);
		createAccount.setOnClickListener(this);
		demoLogin.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		isDemo = false;
		switch (v.getId()) {
		case R.id.btn_login:
			
			if (NetworkUtil.getConnectivityStatus(getApplicationContext())) {
				LoginTask mLoginTask = new LoginTask();
				mLoginTask.execute();
			} else {
				Toast.makeText(LoginActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
			}

			break;
		case R.id.btn_to_forgot_password:
			Intent i = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
			startActivityForResult(i, 101);
			break;
		case R.id.btn_to_create_account:
			Intent z = new Intent(LoginActivity.this, CreateAccountActivity.class);
			startActivityForResult(z, 100);
			break;
		case R.id.demoLogin:
			isDemo = true;
			if (NetworkUtil.getConnectivityStatus(getApplicationContext())) {
				LoginTask mLoginTask = new LoginTask();
				mLoginTask.execute();
			} else {
				Toast.makeText(LoginActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
			}
			
		default:
			break;
		}

	}
	private class LoginTask extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(LoginActivity.this);
			progressDialog.setMessage("Logging in...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			if(!isDemo) {
				String usernameText = username.getText().toString().trim();
				String passwordText = password.getText().toString().trim();
	
				rqts = new RequestToServer(usernameText, passwordText);
			} else {
				rqts = new RequestToServer("demo", "password");
			}
			isDemo = false;
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 1);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();
			String lastLogin = "";
			if (sjta.getJsObjResponse() != null) {
				try {
					if(sjta.getJsObjResponse().has("demofail")) {
						Toast.makeText(LoginActivity.this, "Login Failed! Maximum number of demo users currently logged in. Please wait a few minutes and try again", Toast.LENGTH_SHORT)
						.show();
					}else if (sjta.getJsObjResponse().get("password") != null) {
						
						LoginData mLoginData = new LoginData();
						mLoginData.setId(sjta.getJsObjResponse().get("id").toString());
						mLoginData.setTk_id(sjta.getJsObjResponse().get("tk_id").toString());
						mLoginData.setRole(sjta.getJsObjResponse().get("role").toString());
						mLoginData.setUsername(sjta.getJsObjResponse().get("username").toString());
						mLoginData.setPassword(sjta.getJsObjResponse().get("password").toString());
						String exchange; 
						editor.putString("u_n_new", sjta.getJsObjResponse().get("username").toString());
						editor.commit();
						try{
							exchange = sjta.getJsObjResponse().getString("exchange_email");
							if(exchange == null || exchange.equals("NULL") || exchange.equals("")) {
								exchange = sjta.getJsObjResponse().getString("email");
							}
						}catch(JSONException e) {
							exchange = sjta.getJsObjResponse().getString("email");
						}
						
						try {
							lastLogin =  sjta.getJsObjResponse().getString("last_login");
						} catch(Exception e) {
							lastLogin =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
						}
						
						ObscuredSharedPreferences.Editor o = new ObscuredSharedPreferences(context, context.getSharedPreferences("Precision Preferences", Context.MODE_PRIVATE)).edit();
						o.putString("exchange_email", exchange);
						o.putString("last_login", lastLogin);
						o.commit();
						password.setText("");
						Intent mIntent = new Intent(LoginActivity.this, HomeActivity.class);
						startActivity(mIntent);
						overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);

					}
				} catch (JSONException e) {
					Toast.makeText(LoginActivity.this, "Login Failed! Invalid login credentials, please try again", Toast.LENGTH_SHORT)
							.show();
					Log.e("", "Login Failed !");
				}

			}

			super.onPostExecute(result);

		}
	}

}
