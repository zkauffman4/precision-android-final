package com.precisiontimeentry.precision.model;

import java.io.Serializable;

public class Matter 

implements Serializable{
	 private String matter_name = "";
	 private String matter_id = "";
	 private String task_group = "";
	 private String component_group = "";
	

	 public void setName(String name) {
	  this.matter_name = name;
	 }

	 public String getName() {
	  return matter_name;
	 }

	 public void setMatterId(String id) {
	  this.matter_id = id;
	 }

	 public String getMatterId() {
	  return matter_id;
	 }
	 
	 public void setTaskGroup(String group) {
		 this.task_group = group;
	 }
	 
	 public String getTaskGroup() {
		 return task_group;
	 }
	 
	 public void setComponentGroup(String group) {
		 this.component_group = group;
	 }
	 
	 public String getComponentGroup() {
		 return component_group;
	 }

}