package com.precisiontimeentry.precision.model;

import java.util.ArrayList;
import java.util.TreeSet;

import com.precisiontimeentry.precision.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CallsAdapter extends BaseAdapter {
	 private static ArrayList<Call> searchArrayList = new ArrayList<Call>();
	 private TreeSet<Integer> headerList = new TreeSet<Integer>();
	 
	 private LayoutInflater mInflater;
	 private static final int TYPE_ITEM = 0;
	 private static final int TYPE_SEPARATOR = 1;

	 public CallsAdapter(Context context) {
		 searchArrayList.clear();
		 headerList.clear();
	  mInflater = LayoutInflater.from(context);
	 }

	 public int getCount() {
	  return searchArrayList.size();
	 }

	 public void addItem(final Call c) {
		 searchArrayList.add(c);
		 notifyDataSetChanged();
	 }
	 
	 public Object getItem(int position) {
	  return searchArrayList.get(position);
	 }

	 public long getItemId(int position) {
	  return position;
	 }
	 
	 public int numItemsChecked() {
		 int count = 0;
		 for(int i = 0; i < searchArrayList.size(); i++) {
			 if(searchArrayList.get(i).isChecked()) {
				 count++;
			 }
		 }
		 return count;
	 }
	 
	 public void clearCheckedItems() {
		 for(int i = 0; i < searchArrayList.size(); i++) {
			 searchArrayList.get(i).setChecked(false);
		 }
	 }
	 
	 public void addSectionHeaderItem(final String item) {
		 Call c = new Call();
		 c.setPhoneNumber(item);
		 searchArrayList.add(c);
		 headerList.add(searchArrayList.size() - 1);
		 notifyDataSetChanged();
	 }
	 
	 @Override
	 public int getItemViewType(int position) {
		 return headerList.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
	 }
	 
	 @Override 
	 public int getViewTypeCount() {
		 return 2;
	 }

	 @Override
	 public View getView(int position, View convertView, ViewGroup parent) {
	  ViewHolder holder;
	  int rowType = getItemViewType(position);
	  
	  if (convertView == null) {
		  holder = new ViewHolder();
		  switch(rowType) {
		  case TYPE_ITEM:
			  convertView = mInflater.inflate(R.layout.entry_list_row, null);
			  holder.phoneNumber = (TextView) convertView.findViewById(R.id.matter_name_entry);
			   holder.length = (TextView) convertView.findViewById(R.id.description_entry);
			   holder.date = (TextView) convertView.findViewById(R.id.date_entry);
			   holder.phoneNumber.setText(searchArrayList.get(position).getPhoneNumber());
			   holder.length.setText(searchArrayList.get(position).getLength());
			   holder.date.setText(searchArrayList.get(position).getDate());
			   break;
		  case TYPE_SEPARATOR:
			  convertView = mInflater.inflate(R.layout.section_header, null);
			  holder.phoneNumber = (TextView) convertView.findViewById(R.id.textSeparator);
			  holder.phoneNumber.setText(searchArrayList.get(position).getPhoneNumber());
			  break;
		  }
		  convertView.setTag(holder);
	  } else {
		  holder = (ViewHolder) convertView.getTag();
		  switch(rowType) {
		  case TYPE_ITEM:
			  convertView = mInflater.inflate(R.layout.entry_list_row, null);
			  holder.phoneNumber = (TextView) convertView.findViewById(R.id.matter_name_entry);
			   holder.length = (TextView) convertView.findViewById(R.id.description_entry);
			   holder.date = (TextView) convertView.findViewById(R.id.date_entry);
			   holder.phoneNumber.setText(searchArrayList.get(position).getPhoneNumber());
			   holder.length.setText(searchArrayList.get(position).getLength());
			   holder.date.setText(searchArrayList.get(position).getDate());
			   
			   break;
		  case TYPE_SEPARATOR:
			  convertView = mInflater.inflate(R.layout.section_header, null);
			  holder.phoneNumber = (TextView) convertView.findViewById(R.id.textSeparator);
			  holder.phoneNumber.setText(searchArrayList.get(position).getPhoneNumber());
			  break;
		  }
		  convertView.setTag(holder);
	  } 

	  return convertView;
	 }

	 static class ViewHolder {
	  TextView phoneNumber;
	  TextView date;
	  TextView length;
	 }
}