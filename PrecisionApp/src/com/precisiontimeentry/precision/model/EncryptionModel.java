package com.precisiontimeentry.precision.model;

import java.io.ByteArrayOutputStream;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionModel {
	
	public static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(clear);
        return encrypted;
    }

    public static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }
    
    public static String getEncryptedString(String encryptThis, String key) {
    	
    	try {
	    	ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
	    	byte[] keyStart = key.getBytes();
	    	KeyGenerator keyGen = KeyGenerator.getInstance("AES");
	    	SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
	    	sr.setSeed(keyStart);
	    	keyGen.init(256, sr);
	    	SecretKey secret = keyGen.generateKey();
	    	byte[] res = secret.getEncoded();
	    	return EncryptionModel.encrypt(res, encryptThis.getBytes()).toString();
    	} catch(Exception e) {
    		//no algo
    	}
    	return null;
    }
    
public static String getDecryptedString(String decryptThis, String key) {
    	try {
	    	ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
	    	byte[] keyStart = key.getBytes();
	    	KeyGenerator keyGen = KeyGenerator.getInstance("AES");
	    	SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
	    	sr.setSeed(keyStart);
	    	keyGen.init(256, sr);
	    	SecretKey secret = keyGen.generateKey();
	    	byte[] res = secret.getEncoded();
	    	return EncryptionModel.decrypt(res, decryptThis.getBytes()).toString();
    	} catch(Exception e) {
    		//no algo
    	}
    	
    	return null;
    	
    }
}
