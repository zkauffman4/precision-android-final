package com.precisiontimeentry.precision.model;

import java.util.ArrayList;

import com.precisiontimeentry.precision.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class EntryAdapter extends BaseAdapter {
	 private static ArrayList<Entry> searchArrayList;
	 
	 private LayoutInflater mInflater;

	 public EntryAdapter(Context context, ArrayList<Entry> results) {
	  searchArrayList = results;
	  mInflater = LayoutInflater.from(context);
	 }

	 public int getCount() {
	  return searchArrayList.size();
	 }

	 public Object getItem(int position) {
	  return searchArrayList.get(position);
	 }

	 public long getItemId(int position) {
	  return position;
	 }

	 @Override
	 public View getView(int position, View convertView, ViewGroup parent) {
	  ViewHolder holder;
	  if (convertView == null) {
	   convertView = mInflater.inflate(R.layout.entry_list_row, null);
	   holder = new ViewHolder();
	   holder.matterName = (TextView) convertView.findViewById(R.id.matter_name_entry);
	   holder.matterDesc = (TextView) convertView.findViewById(R.id.description_entry);
	   holder.date = (TextView) convertView.findViewById(R.id.date_entry);
	   holder.hours = (TextView) convertView.findViewById(R.id.hours_entry);

	   convertView.setTag(holder);
	  } else {
	   holder = (ViewHolder) convertView.getTag();
	  }
	  
	  holder.matterName.setText(searchArrayList.get(position).getName());
	  holder.matterDesc.setText(searchArrayList.get(position).getDescription());
	  holder.date.setText(searchArrayList.get(position).getDate());
	  holder.hours.setText(searchArrayList.get(position).getHours());
	  

	  return convertView;
	 }

	 static class ViewHolder {
	  TextView matterName;
	  TextView matterDesc;
	  TextView date;
	  TextView hours;
	 }
}