package com.precisiontimeentry.precision.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Call implements Serializable {

	private String phoneNumber, date, length;
	private int minutes, seconds, hours;
	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	private boolean checked = false;
	
	public Call() {
		this.checked = false;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public int getSeconds() {
		return seconds;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDate() {
		return date;
	}

	public void setDate(long date) {
		SimpleDateFormat formatter = new SimpleDateFormat("M/dd/yyyy hh:mm a");
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(date);
		
		this.date = formatter.format(cal.getTime());
	}

	public String getLength() {
		return length;
	}

	public void setLength(int length) {
		int hours = length/3600;
		int minutes = (length - (hours*3600))/60;
		int seconds = (length - hours*3600 - minutes*60);
		this.setMinutes(minutes);
		this.setSeconds(seconds);
		this.setHours(hours);
		this.length = minutes + " minute(s) and "+seconds+" second(s)";
		if(hours != 0) {
			this.length = hours + " hour(s) " + this.length;
		}
	}
	
}
