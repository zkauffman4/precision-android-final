package com.precisiontimeentry.precision.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Entry 

implements Serializable{
	 private String matter_name = "";
	 private String matter_id = "";
	 private String task_group = "";
	 private String component_group = "";
	 private String task_code_id = "";
	 private String component_code_id = "";
	 private String hours = "";
	 private String date = "", formattedDate = "";
	 private String description = "";
	 private String minutes = "";
	 private boolean isOpenedToLeft;
	 private boolean isOpenedToRight = isOpenedToLeft = false;
	 private String id, te;
	

	 public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isOpenedToLeft() {
		return isOpenedToLeft;
	}

	public void setOpenedToLeft(boolean isOpenedToLeft) {
		this.isOpenedToLeft = isOpenedToLeft;
	}

	public boolean isOpenedToRight() {
		return isOpenedToRight;
	}

	public void setOpenedToRight(boolean isOpenedToRight) {
		this.isOpenedToRight = isOpenedToRight;
	}

	public String getMinutes() {
		return minutes;
	}

	public void setMinutes(String minutes) {
		this.minutes = minutes;
	}

	public String getTask_code_id() {
		 if(task_code_id == null || task_code_id == "null") {
			 task_code_id = "";
		 }
		return task_code_id;
	}

	public void setTask_code_id(String task_code_id) {
		this.task_code_id = task_code_id;
	}

	public String getComponent_code_id() {
		if(component_code_id == null || component_code_id == "null") {
			component_code_id = "";
		 }
		return component_code_id;
	}

	public void setComponent_code_id(String component_code_id) {
		this.component_code_id = component_code_id;
	}

	public String getHours() {
		if(hours == null || hours == "null") {
			hours = "";
		 }
		return hours;
	}

	public void setHours(String hours) {
		this.hours = hours;
	}

	public String getDate() {
		if(date == null || date == "null") {
			date = "";
		 }
		return date;
	}
	
	public String getDBFormattedDate() {
		if(date == null || date == "null") {
			date = "";
		 }
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date d = sdf.parse(this.date);
			sdf.applyPattern("yyyy-MM-dd");
			String newDateString = sdf.format(d);
			return newDateString;
		}catch(Exception e) {
			return date;
		}
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public String getFormattedDate() {
		return this.formattedDate;
	}

	public String getDescription() {
		if(description == null || description == "null") {
			description = "";
		 }
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
	  this.matter_name = name;
	 }

	 public String getName() {
		 if(matter_name == null || matter_name == "null") {
				matter_name = "";
			 }
	  return matter_name;
	 }

	 public void setMatterId(String id) {
	  this.matter_id = id;
	 }

	 public String getMatterId() {
		 if(matter_id == null || matter_id == "null") {
				matter_id = "";
			 }
	  return matter_id;
	 }
	 
	 public void setTaskGroup(String group) {
		 this.task_group = group;
	 }
	 
	 public String getTaskGroup() {
		 if(task_group == null || task_group == "null") {
				task_group = "";
			 }
		 return task_group;
	 }
	 
	 public void setComponentGroup(String group) {
		 this.component_group = group;
	 }
	 
	 public String getComponentGroup() {
		 if(component_group == null || component_group == "null") {
			 component_group = "";
			 }
		 return component_group;
	 }

	public String getTe() {
		return te;
	}

	public void setTe(String te) {
		this.te = te;
	}

}