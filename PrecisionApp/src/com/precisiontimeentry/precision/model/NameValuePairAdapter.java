package com.precisiontimeentry.precision.model;

import java.util.ArrayList;

import org.apache.http.NameValuePair;

import com.precisiontimeentry.precision.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class NameValuePairAdapter extends BaseAdapter {
	 private static ArrayList<Matter> searchArrayList;
	 
	 private LayoutInflater mInflater;

	 public NameValuePairAdapter(Context context, ArrayList<Matter> results) {
	  searchArrayList = results;
	  mInflater = LayoutInflater.from(context);
	 }

	 public int getCount() {
	  return searchArrayList.size();
	 }

	 public Object getItem(int position) {
	  return searchArrayList.get(position);
	 }

	 public long getItemId(int position) {
	  return position;
	 }

	 @Override
	 public View getView(int position, View convertView, ViewGroup parent) {
	  ViewHolder holder;
	  if (convertView == null) {
	   convertView = mInflater.inflate(R.layout.matter_search_row, null);
	   holder = new ViewHolder();
	   holder.matterName = (TextView) convertView.findViewById(R.id.matter_name);
	   holder.matterId = (TextView) convertView.findViewById(R.id.matter_id);

	   convertView.setTag(holder);
	  } else {
	   holder = (ViewHolder) convertView.getTag();
	  }
	  
	  holder.matterName.setText(searchArrayList.get(position).getName());
	  holder.matterId.setText(searchArrayList.get(position).getMatterId());

	  return convertView;
	 }

	 static class ViewHolder {
	  TextView matterName;
	  TextView matterId;
	 }
}