package com.precisiontimeentry.precision.model;

import java.util.List;

import com.precisiontimeentry.precision.PendingTabsActivity;
import com.precisiontimeentry.precision.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
 
public class ItemAdapter extends ArrayAdapter {
 
      List   data;
      Context context;
      Activity activity;
      int layoutResID;
 
public ItemAdapter(Context context, Activity activity, int layoutResourceId,List data) {
      super(context, layoutResourceId, data);
 
      this.data=data;
      this.context=context;
      this.layoutResID=layoutResourceId;
      this.activity = activity;
 
      // TODO Auto-generated constructor stub
}
 
@Override
public View getView(final int position, View convertView, ViewGroup parent) {
 
      NewsHolder holder = null;
         View row = convertView;
          holder = null;
 
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResID, parent, false);
 
            holder = new NewsHolder();
 
            holder.matterName = (TextView)row.findViewById(R.id.matter_name_entry);
            holder.description = (TextView)row.findViewById(R.id.description_entry);
            holder.date = (TextView)row.findViewById(R.id.date_entry);
            holder.hours = (TextView)row.findViewById(R.id.hours_entry);
            holder.button1=(RelativeLayout)row.findViewById(R.id.swipe_button1);
            holder.button2=(RelativeLayout)row.findViewById(R.id.swipe_button2);
            holder.button3=(RelativeLayout)row.findViewById(R.id.swipe_button3);
            row.setTag(holder);
        }
        else
        {
            holder = (NewsHolder)row.getTag();
        }
 
        final Entry itemdata = (Entry)data.get(position);
        holder.matterName.setText(itemdata.getName());
        holder.description.setText(itemdata.getDescription());
        holder.date.setText(itemdata.getDate());
        holder.hours.setText(itemdata.getHours());
        //holder.icon.setImageDrawable(itemdata.getIcon());
 
        holder.button1.setOnClickListener(new View.OnClickListener() {
 
                  @Override
                  public void onClick(View v) {
                        // TODO Auto-generated method stub
                	  if(itemdata.isOpenedToRight()) {
                		  //delete itemdata entry
                		  String id = itemdata.getId();
                		  String arraySpot = String.valueOf(position);
                		  ((PendingTabsActivity)activity).deleteEntry(id, arraySpot);
                	  }
                        
                  }
            });
 
             holder.button2.setOnClickListener(new View.OnClickListener() {
 
                              @Override
                              public void onClick(View v) {
                                    // TODO Auto-generated method stub
                            	  if(itemdata.isOpenedToLeft()) {
                            		  //submit entry
                            		  String id = itemdata.getId();
                            		  String arraySpot = String.valueOf(position);
                            		  ((PendingTabsActivity)activity).submitEntry(id, arraySpot);
                            	  }
                                    
                              }
                        });
 
             holder.button3.setOnClickListener(new View.OnClickListener() {
 
                        @Override
                        public void onClick(View v) {
                              // TODO Auto-generated method stub
                        	if(itemdata.isOpenedToLeft()) {
                        		//edit entry
                        		((PendingTabsActivity)activity).editEntry(itemdata);
                        	}
                              
                        }
                  });
 
        return row;
 
}
 
static class NewsHolder{
 
      TextView matterName;
      TextView description;
      TextView date;
      TextView hours;
      RelativeLayout button1;
      RelativeLayout button2;
      RelativeLayout button3;
      }
 
}