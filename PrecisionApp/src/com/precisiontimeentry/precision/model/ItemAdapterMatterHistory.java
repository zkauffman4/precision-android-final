package com.precisiontimeentry.precision.model;

import java.util.List;

import com.precisiontimeentry.precision.PendingTabsActivity;
import com.precisiontimeentry.precision.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
 
public class ItemAdapterMatterHistory extends ArrayAdapter {
 
      List   data;
      Context context;
      int layoutResID;
      private Activity activity;
 
public ItemAdapterMatterHistory(Context context, Activity activity, int layoutResourceId,List data) {
      super(context, layoutResourceId, data);
 
      this.data=data;
      this.context=context;
      this.layoutResID=layoutResourceId;
      this.activity = activity;
 
      // TODO Auto-generated constructor stub
}
 
@Override
public View getView(final int position, View convertView, ViewGroup parent) {
 
      NewsHolder holder = null;
         View row = convertView;
          holder = null;
 
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResID, parent, false);
 
            holder = new NewsHolder();
 
            holder.matterName = (TextView)row.findViewById(R.id.matter_name_history);
            holder.matterId = (TextView)row.findViewById(R.id.matter_id_history);
            row.setTag(holder);
        }
        else
        {
            holder = (NewsHolder)row.getTag();
        }
 
        final Matter itemdata = (Matter)data.get(position);
        holder.matterName.setText(itemdata.getName());
        holder.matterId.setText(itemdata.getMatterId());
       
 
        return row;
 
}
 
static class NewsHolder{
 
      TextView matterName;
      TextView matterId;
      }
 
}