package com.precisiontimeentry.precision;

import java.io.ByteArrayOutputStream;
import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.json.JSONException;

import com.precisiontimeentry.precision.model.ObscuredSharedPreferences;
import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

public class SettingActivity extends Activity {

	private ToggleButton enableEmails, enableLogs, enableWeeklyTracking, zeroTimeToggle;
	private EditText exchangeEmail, exchangePassword;
	private SeekBar goalBar;
	private Button saveBtn;
	private TextView sliderValueLabel;
	private LinearLayout sliderLayout;
	private String oldEmail, oldPassword;
	private ObscuredSharedPreferences preferences;
	private ObscuredSharedPreferences.Editor editor;
	private ProgressDialog progressDialog;
	private boolean didConnect;
	private ImageView backBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		didConnect = false;
		setContentView(R.layout.activity_settings);
		oldEmail = "";
		oldPassword = "";
		enableEmails = (ToggleButton) findViewById(R.id.enableEmailLogsSwitch);
		enableLogs = (ToggleButton) findViewById(R.id.enableCallLogsSwitch);
		enableWeeklyTracking = (ToggleButton) findViewById(R.id.enableGoalTrackingSwitch);
		zeroTimeToggle = (ToggleButton) findViewById(R.id.zeroTimeToggle);
		
		exchangeEmail = (EditText) findViewById(R.id.exchangeEmailTextfield);
		exchangePassword = (EditText) findViewById(R.id.exchangePasswordTextfield);
		
		goalBar = (SeekBar) findViewById(R.id.weeklyGoalSlider);

		sliderValueLabel = (TextView) findViewById(R.id.sliderValueLabel);
		
		saveBtn = (Button) findViewById(R.id.settingsSaveBtn);
		
		sliderLayout = (LinearLayout) findViewById(R.id.sliderLayout);
		
		backBtn = (ImageView) findViewById(R.id.backBtnBySettings);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(-1);
				finish();
			}
			
		});
		
		preferences = new ObscuredSharedPreferences(this, this.getSharedPreferences("Precision Preferences", Context.MODE_PRIVATE)); 
		editor = preferences.edit();
		
		enableEmails.setChecked(preferences.getBoolean("enableEmails", false));
		enableLogs.setChecked(preferences.getBoolean("enableLogs", false));
		enableWeeklyTracking.setChecked(preferences.getBoolean("enableWeeklyTracking", false));
		zeroTimeToggle.setChecked(preferences.getBoolean("zeroTime", false));
		
		enableEmails.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton button, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked) {
					String exchange_email = preferences.getString("exchange_email", "");
					String exchange_pass = preferences.getString("exchange_pass", "");
					if(!exchange_email.equals("null"))
						exchangeEmail.setText(exchange_email);
					exchangePassword.setText(exchange_pass);
					exchangeEmail.setVisibility(View.VISIBLE);
					exchangePassword.setVisibility(View.VISIBLE);
				} else {
					exchangeEmail.setVisibility(View.GONE);
					exchangePassword.setVisibility(View.GONE);
				}
			}
			
		});
		
		enableLogs.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked) {
					//handle button checked
				} else {
					//handle button unchecked
				}
			}
			
		});
		
		goalBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				// TODO Auto-generated method stub
				sliderValueLabel.setText(arg1 + " hours");
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		enableWeeklyTracking.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked) {
					int sliderVal = preferences.getInt("weekly_goal_value", 40);
					goalBar.setProgress(sliderVal);
					sliderValueLabel.setText(sliderVal + " hours");
					sliderLayout.setVisibility(View.VISIBLE);
				} else {
					sliderLayout.setVisibility(View.GONE);
				}
			}
			
		});
		
		
		
		if(!enableEmails.isChecked()) {
			exchangeEmail.setVisibility(View.GONE);
			exchangePassword.setVisibility(View.GONE);
		} else {
			String exchange_email = preferences.getString("exchange_email", "");
			String exchange_pass = preferences.getString("e", "");
			exchangeEmail.setText(exchange_email);
			exchangePassword.setText(exchange_pass);
			oldEmail = exchange_email;
			oldPassword = exchange_pass;
		}
		
		if(!enableWeeklyTracking.isChecked()) {
			sliderLayout.setVisibility(View.GONE);
		} else {
			int sliderVal = preferences.getInt("weekly_goal_value", 40);
			goalBar.setProgress(sliderVal);
			sliderValueLabel.setText(sliderVal + " hours");
		}
		
		saveBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				editor.putBoolean("enableEmails", enableEmails.isChecked());
				editor.putBoolean("enableLogs", enableLogs.isChecked());
				editor.putBoolean("enableWeeklyTracking", enableWeeklyTracking.isChecked());
				editor.putBoolean("zeroTime", zeroTimeToggle.isChecked());
				if(preferences.getBoolean("enableWeeklyTracking", false)) {
					editor.putInt("weekly_goal_value", goalBar.getProgress());
				}
				
				if(enableEmails.isChecked()) {
					
					if(!exchangePassword.getText().toString().equals(oldPassword)) {
						String pw = exchangePassword.getText().toString();
						editor.putString("e", pw);
						
					}
					
					if(!exchangeEmail.getText().toString().equals(oldEmail)) {
						//update email
						oldEmail = exchangeEmail.getText().toString();
						editor.putString("exchange_email", oldEmail);
						UpdateEmail e = new UpdateEmail();
						e.execute();
						didConnect = true;
					}
					editor.putInt("weekly_goal_value", goalBar.getProgress());
				}
				editor.commit();
				if(!didConnect) {
					finish();
				}
			}
		});
		
	}
	
	private class UpdateEmail extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(SettingActivity.this);
			progressDialog.setMessage("Updating email...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			rqts = new RequestToServer("update_email");
			rqts.setPostParam("email", oldEmail);
			
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 1);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();
			if(sjta.getJsObjResponse() != null) {
				if(sjta.getJsObjResponse().has("session")) {
					Intent mIntent = new Intent(SettingActivity.this, LoginActivity.class);
					mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					mIntent.putExtra("invalid_session", true);
					startActivity(mIntent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
					finish();
					return;
				} 
			}
			
			finish();
			/*
			if (sjta.getJsObjResponse() != null) {
				try {
					if (sjta.getJsObjResponse().get("num_codes") != null) {

					}
				} catch (JSONException e) {
					
				}

			}

			super.onPostExecute(result);

		}
		*/
		}
	}

}