package com.precisiontimeentry.precision;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CreateAccountActivity extends Activity {
	
	private EditText _emailTextfield, _usernameTextfield, _billingIdTextfield, 
		_confirmEmailTextfield, _passwordTextfield, _confirmPasswordTextfield, _firmCodeTextfield;
	private Button _create, _back;
	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private Matcher matcher;
	private Pattern pattern;
	private ProgressDialog progressDialog;
	private String username, billingId, email, password, firmCode;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_register);
		
		_emailTextfield = (EditText) findViewById(R.id.txt_create_email);
		_usernameTextfield = (EditText) findViewById(R.id.txt_create_username);
		_billingIdTextfield = (EditText) findViewById(R.id.txt_create_billing_id);
		_confirmEmailTextfield = (EditText) findViewById(R.id.txt_create_confirm_email);
		_passwordTextfield = (EditText) findViewById(R.id.txt_create_password);
		_confirmPasswordTextfield = (EditText) findViewById(R.id.txt_create_confirm_password);
		_firmCodeTextfield = (EditText) findViewById(R.id.txt_create_firm_code);
		
		_create = (Button) findViewById(R.id.btn_create_account);
		_back = (Button) findViewById(R.id.btn_create_to_login);
		
		_create.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//check fields and attempt reset
				pattern = Pattern.compile(EMAIL_PATTERN);
				matcher = pattern.matcher(_emailTextfield.getText().toString());
				if(_usernameTextfield.getText().toString().length() == 0) {
					Toast.makeText(CreateAccountActivity.this, "Username cannot be empty!", Toast.LENGTH_SHORT).show();
				}else if(_billingIdTextfield.getText().toString().length() == 0) {
					Toast.makeText(CreateAccountActivity.this, "Billing ID cannot be empty!", Toast.LENGTH_SHORT).show();
				}else if(_emailTextfield.getText().toString().length() == 0) {
					Toast.makeText(CreateAccountActivity.this, "Email cannot be empty!", Toast.LENGTH_SHORT).show();
				}else if(_confirmEmailTextfield.getText().toString().length() == 0) {
					Toast.makeText(CreateAccountActivity.this, "Email confirmation cannot be empty!", Toast.LENGTH_SHORT).show();
				}else if(_passwordTextfield.getText().toString().length() == 0) {
					Toast.makeText(CreateAccountActivity.this, "Password cannot be empty!", Toast.LENGTH_SHORT).show();
				}else if(_confirmPasswordTextfield.getText().toString().length() == 0) {
					Toast.makeText(CreateAccountActivity.this, "Password confirmation cannot be empty!", Toast.LENGTH_SHORT).show();
				}else if(_firmCodeTextfield.getText().toString().length() == 0) {
					Toast.makeText(CreateAccountActivity.this, "Firm code cannot be empty!", Toast.LENGTH_SHORT).show();
				}else if(!matcher.matches()) {
					Toast.makeText(CreateAccountActivity.this, "Email is invalid!", Toast.LENGTH_SHORT).show();
				}else if(!_emailTextfield.getText().toString().equals(_confirmEmailTextfield.getText().toString())) {
					Toast.makeText(CreateAccountActivity.this, "Emails do not match!", Toast.LENGTH_SHORT).show();
				}else if(!_passwordTextfield.getText().toString().equals(_confirmPasswordTextfield.getText().toString())) {
					Toast.makeText(CreateAccountActivity.this, "Passwords do not match!", Toast.LENGTH_SHORT).show();
				}else if(!(_passwordTextfield.getText().toString().length() > 6)) {
					Toast.makeText(CreateAccountActivity.this, "Password must be at least 7 characters!", Toast.LENGTH_SHORT).show();
				} else {
					//try to server
					email = _emailTextfield.getText().toString();
					username = _usernameTextfield.getText().toString();
					billingId = _billingIdTextfield.getText().toString();
					password = _passwordTextfield.getText().toString();
					firmCode = _firmCodeTextfield.getText().toString();
					
					CreateAccount c = new CreateAccount();
					c.execute();
				}
			}
			
		});
		
		_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setResult(-1);
				finish();
			}
			
		});
		
	}
	
	private class CreateAccount extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(CreateAccountActivity.this);
			progressDialog.setMessage("Creating account...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			rqts = new RequestToServer("create_account");
			rqts.setAccountCreation(username, email, billingId, password, firmCode);

			sjta = new SenderToJSONAPI(rqts, 1001, 1);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			if (sjta.getJsObjResponse() != null) {
				try {
					int code = sjta.getJsObjResponse().getInt("response_code");
					switch(code) {
					case 1:
						Toast.makeText(CreateAccountActivity.this, "Username already exists. Please change and try again!", Toast.LENGTH_SHORT).show();
						break;
					case 2:
						Toast.makeText(CreateAccountActivity.this, "Email already exists. Please change and try again!", Toast.LENGTH_SHORT).show();
						break;
					case 3:
						Toast.makeText(CreateAccountActivity.this, "Invalid firm registration code. Please change and try again!", Toast.LENGTH_SHORT).show();
						break;
					case 4:
						Toast.makeText(CreateAccountActivity.this, "Database error. Please change and try again!", Toast.LENGTH_SHORT).show();
						break;
					case 5:
						setResult(100);
						finish();
						break;
					}
				} catch (JSONException e) {
					
				}

			}

			super.onPostExecute(result);

		}
	}

}
