package com.precisiontimeentry.precision;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.precisiontimeentry.precision.model.Entry;
import com.precisiontimeentry.precision.model.EntryAdapter;
import com.precisiontimeentry.precision.model.ItemAdapter;
import com.precisiontimeentry.precision.model.ItemAdapterNoSubmit;
import com.precisiontimeentry.precision.model.NameValuePairAdapter;
import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class PendingTabsActivity extends Activity implements OnClickListener {

	private Button RtoSButton, IncButton, LogButton;
	private ProgressDialog progressDialog;
	private SwipeListView selectedList;
	private ArrayList<Entry> entriesArray;
	public int listEnum;
	private ItemAdapter adapter;
	private ItemAdapterNoSubmit adapterNS;
	private boolean isOpenedToRight;
	private boolean isListItemOpen;
	private Activity activity;
	private String operation, arraySpot, id;
	private int mPositionOfOpened, unselected, selected;
	private ImageView backBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mPositionOfOpened = -1;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pending_entries_tabs);
		activity = this;
		isOpenedToRight = false;
		isListItemOpen = false;
		RtoSButton = (Button) findViewById(R.id.btn_rtos);
		IncButton = (Button) findViewById(R.id.btn_incmp);
		LogButton = (Button) findViewById(R.id.btn_log_entries);
		unselected = RtoSButton.getTextColors().getDefaultColor();
		selected = Color.rgb(0, 122, 255);
		RtoSButton.setTextColor(selected);
		RtoSButton.setOnClickListener(this);
		IncButton.setOnClickListener(this);
		LogButton.setOnClickListener(this);
		selectedList = (SwipeListView) findViewById(R.id.example_swipe_lv_list);
		entriesArray = new ArrayList<Entry>();
		listEnum = 1;
		
		backBtn = (ImageView) findViewById(R.id.backBtnPendingEntries);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(-1);
				finish();
			}
			
		});
		
		selectedList.setSwipeListViewListener(new BaseSwipeListViewListener() {
	         @Override
	         public void onOpened(int position, boolean toRight) {
	        	 entriesArray.get(position).setOpenedToLeft(!toRight);
	             entriesArray.get(position).setOpenedToRight(toRight);
	        	 if(mPositionOfOpened != -1 && mPositionOfOpened < entriesArray.size() && mPositionOfOpened != position) {
	        		 selectedList.closeAnimate(mPositionOfOpened);
	        	 }
	        	 mPositionOfOpened = position;
	         }
	 
	         @Override
	         public void onClosed(int position, boolean fromRight) {
	        	 isListItemOpen = false;
	         }
	 
	         @Override
	         public void onListChanged() {
	         }
	 
	         @Override
	         public void onMove(int position, float x) {
	         }
	 
	         @Override
	         public void onStartOpen(int position, int action, boolean right) {
	        
	             entriesArray.get(position).setOpenedToLeft(!right);
	             entriesArray.get(position).setOpenedToRight(right);
	         }
	 
	         @Override
	         public void onStartClose(int position, boolean right) {
	             Log.d("swipe", String.format("onStartClose %d", position));
	         }
	 
	         @Override
	         public void onClickFrontView(int position) {
	             Log.d("swipe", String.format("onClickFrontView %d", position));
	 
	             selectedList.openAnimate(position); //when you touch front view it will open
	 
	         }
	 
	         @Override
	         public void onClickBackView(int position) {
	             Log.d("swipe", String.format("onClickBackView %d", position));
	 
	             selectedList.closeAnimate(position);//when you touch back view it will close
	         }
	 
	         @Override
	         public void onDismiss(int[] reverseSortedPositions) {
	 
	         }
	 
	     });
		
		
		Display display = getWindowManager().getDefaultDisplay();
	    DisplayMetrics outMetrics = new DisplayMetrics ();
	    display.getMetrics(outMetrics);

	    float density  = getResources().getDisplayMetrics().density;
	    float dpHeight = outMetrics.heightPixels / density;
	    float dpWidth  = outMetrics.widthPixels / density;
		
		selectedList.setSwipeMode(SwipeListView.SWIPE_MODE_BOTH); // there are five swiping modes
		selectedList.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL); //there are four swipe actions
		selectedList.setSwipeActionRight(SwipeListView.SWIPE_ACTION_REVEAL);
		selectedList.setOffsetLeft(convertDpToPixel(dpWidth*.33f)); // left side offset
		selectedList.setOffsetRight(convertDpToPixel(dpWidth*.67f)); // right side offset
		selectedList.setAnimationTime(50); // animarion time
		selectedList.setSwipeOpenOnLongPress(false); // enable or disable SwipeOpenOnLongPress
		
		
		GetSubmittables gs = new GetSubmittables();
		gs.execute();

	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == 202) {
			GetSubmittables gs = new GetSubmittables();
			gs.execute();
		}
	}
	
	public void deleteEntry(String id, String arraySpot) {
		operation = "delete";
		this.id = id;
		this.arraySpot = arraySpot;
		DoOperation d = new DoOperation();
		d.execute();
	}
	
	public void submitEntry(String id, String arraySpot) {
		operation = "submit";
		this.id = id;
		this.arraySpot = arraySpot;
		DoOperation d = new DoOperation();
		d.execute();
	}
	
	public void editEntry(Entry e) {
		Intent i = new Intent(PendingTabsActivity.this, NewEntryActivity.class);
		i.putExtra("editingEntry", e);
		startActivityForResult(i, 202);
	}

	@Override
	public void onClick(View v) {
		entriesArray.clear();
		GetSubmittables gs = new GetSubmittables();
		Display display = getWindowManager().getDefaultDisplay();
	    DisplayMetrics outMetrics = new DisplayMetrics ();
	    display.getMetrics(outMetrics);

	    float density  = getResources().getDisplayMetrics().density;
	    float dpHeight = outMetrics.heightPixels / density;
	    float dpWidth  = outMetrics.widthPixels / density;
		//Fragment fragment = null;
		switch (v.getId()) {
		case R.id.btn_rtos:
			RtoSButton.setTextColor(selected);
			IncButton.setTextColor(unselected);
			LogButton.setTextColor(unselected);
			listEnum = 1;
			selectedList.setOffsetLeft(convertDpToPixel(dpWidth*.33f)); // left side offset
			selectedList.setOffsetRight(convertDpToPixel(dpWidth*.67f)); // right side offset
			break;
		case R.id.btn_incmp:
			listEnum = 2;
			RtoSButton.setTextColor(unselected);
			IncButton.setTextColor(selected);
			LogButton.setTextColor(unselected);
			selectedList.setOffsetLeft(convertDpToPixel(dpWidth*.5f)); // left side offset
			selectedList.setOffsetRight(convertDpToPixel(dpWidth*.5f)); // right side offset
			break;
		case R.id.btn_log_entries:
			listEnum = 3;
			RtoSButton.setTextColor(unselected);
			IncButton.setTextColor(unselected);
			LogButton.setTextColor(selected);
			selectedList.setOffsetLeft(convertDpToPixel(dpWidth*.5f)); // left side offset
			selectedList.setOffsetRight(convertDpToPixel(dpWidth*.5f)); // right side offset
			break;
		default:
			break;
		}
		mPositionOfOpened = -1;
		gs.execute();
		/*
		if (fragment != null) {
			mFragmentManager = getSupportFragmentManager();
			mFragmentTransaction = mFragmentManager.beginTransaction();
			mFragmentTransaction.replace(R.id.frame_container, fragment);
			mFragmentTransaction.commit();
		}
		*/
	}
	
	public int convertDpToPixel(float dp) {
	       DisplayMetrics metrics = getResources().getDisplayMetrics();
	       float px = dp * (metrics.densityDpi / 160f);
	       return (int) px;
	   }
	
	private class GetSubmittables extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(PendingTabsActivity.this);
			progressDialog.setMessage("Retrieving entries...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

		
			if(listEnum == 1) {
				rqts = new RequestToServer("get_submittables");
			} else if(listEnum == 2) {
				rqts = new RequestToServer("get_incompletes");
			} else if(listEnum == 3) {
				rqts = new RequestToServer("get_log_entries");
			}
			
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1002, 0);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();
			if(sjta.getJsObjResponse() != null) {
				if(sjta.getJsObjResponse().has("session")) {
					Intent mIntent = new Intent(PendingTabsActivity.this, LoginActivity.class);
					mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					mIntent.putExtra("invalid_session", true);
					startActivity(mIntent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
					finish();
					return;
				} 
			}else if (sjta.getJsMassResponse() != null) {
				try {
					entriesArray.clear();
					JSONArray entries = sjta.getJsMassResponse();
					for(int i = 0; i < entries.length(); i++) {
						JSONObject o = entries.getJSONObject(i);
						String matterId = o.getString("matter_id");
						String matterName = o.getString("matter_name");
						String description = o.getString("description");
						String taskCode = o.getString("task_code_id");
						String componentCode = o.getString("component_code_id");
						String hours = o.getString("hours_worked");
						String date = o.getString("date_worked");
						String tGroup = o.getString("task_group");
						String cGroup = o.getString("component_group");
						String id = o.getString("id");
						Entry e = new Entry();
						e.setId(id);
						e.setComponent_code_id(componentCode);
						e.setComponentGroup(cGroup);
						e.setDate(date);
						e.setDescription(description);
						e.setHours(hours);
						e.setMatterId(matterId);
						e.setName(matterName);
						e.setTask_code_id(taskCode);
						e.setTaskGroup(tGroup);
						e.setTe(String.valueOf(listEnum));
						
						entriesArray.add(e);
					}

				} catch (JSONException e) {
					Toast.makeText(PendingTabsActivity.this, "Could not retrieve components, please try again", Toast.LENGTH_SHORT)
							.show();
					Log.e("", "Component search failed !");
				}

			}

			super.onPostExecute(result);
			if(listEnum == 1) {
				adapter = new ItemAdapter(PendingTabsActivity.this, activity, R.layout.swipeable_row, entriesArray);
				selectedList.setAdapter(adapter);
				adapter.notifyDataSetChanged();
			} else if(listEnum == 2 || listEnum == 3) {
				adapterNS = new ItemAdapterNoSubmit(PendingTabsActivity.this, activity, R.layout.swipeable_row_no_submit, entriesArray);
				selectedList.setAdapter(adapterNS);
				adapterNS.notifyDataSetChanged();
			} 
			
				//selectedList.setAdapter(new EntryAdapter(PendingTabsActivity.this, entriesArray));
		}
	}
	
	private class DoOperation extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(PendingTabsActivity.this);
			if(operation.equals("delete")) {
				progressDialog.setMessage("Deleting entry...");
			} else {
				progressDialog.setMessage("Submitting entry...");
			}
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

		
			if(operation.equals("delete")) {
				rqts = new RequestToServer("delete_entry");
				if(listEnum == 3) {
					rqts.setEntryIdListArraySpot(id, String.valueOf(2), arraySpot);
				} else {
					rqts.setEntryIdListArraySpot(id, String.valueOf(listEnum), arraySpot);
				}
				
			} else if(operation.equals("submit")) {
				rqts = new RequestToServer("submit_entry");
				rqts.setEntryIdListArraySpot(id, "", arraySpot);
			}
			
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 1);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			if (sjta.getJsObjResponse() != null) {
				try {
					if(sjta.getJsObjResponse().has("session")) {
						Intent mIntent = new Intent(PendingTabsActivity.this, LoginActivity.class);
						mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
						mIntent.putExtra("invalid_session", true);
						startActivity(mIntent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
						finish();
						return;
					} else if(operation.equals("delete")) {
						int spot = sjta.getJsObjResponse().getInt("success");
						entriesArray.remove(spot);
					} else if(operation.equals("submit")) {
						int spot = sjta.getJsObjResponse().getInt("submit");
						entriesArray.remove(spot);
					}
					

				} catch (JSONException e) {
					Toast.makeText(PendingTabsActivity.this, "Could not retrieve components, please try again", Toast.LENGTH_SHORT)
							.show();
					Log.e("", "Component search failed !");
				}

			}

			super.onPostExecute(result);
			if(listEnum == 1) {
				adapter = new ItemAdapter(PendingTabsActivity.this, activity, R.layout.swipeable_row, entriesArray);
				selectedList.setAdapter(adapter);
				adapter.notifyDataSetChanged();
			} else if(listEnum == 2 || listEnum == 3) {
				adapterNS = new ItemAdapterNoSubmit(PendingTabsActivity.this, activity, R.layout.swipeable_row_no_submit, entriesArray);
				selectedList.setAdapter(adapterNS);
				adapterNS.notifyDataSetChanged();
			}
			
				//selectedList.setAdapter(new EntryAdapter(PendingTabsActivity.this, entriesArray));
		}
	}

}