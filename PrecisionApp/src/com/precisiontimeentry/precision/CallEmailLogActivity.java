package com.precisiontimeentry.precision;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.precisiontimeentry.precision.model.Call;
import com.precisiontimeentry.precision.model.CallsAdapter;
import com.precisiontimeentry.precision.model.Email;
import com.precisiontimeentry.precision.model.EmailAdapter;
import com.precisiontimeentry.precision.model.Entry;
import com.precisiontimeentry.precision.model.EntryAdapter;
import com.precisiontimeentry.precision.model.Matter;
import com.precisiontimeentry.precision.model.NameValuePairAdapter;
import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ListView;

public class CallEmailLogActivity extends Activity {

	public ArrayList<Email> inbox;
	public ArrayList<Email> sent;
	public ArrayList<Email> allEmails;
	public ListView logListView;
	public EmailAdapter adapter;
	public int inboxCount;
	public ArrayList<Call> sentCalls, receivedCalls, allCalls;
	public Button emailsButton, callsButton, createButton;
	private int selected;
	private int unselected;
	private int listEnum;
	private ArrayList<Call> selectedCalls = new ArrayList<Call>();
	private ArrayList<Email> selectedEmails = new ArrayList<Email>();
	private ProgressDialog progressDialog;
	private JSONArray submittable;
	private ImageView backBtn;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent i = getIntent();
		inboxCount = 0;
		inbox = (ArrayList<Email>)i.getSerializableExtra("inbox");
		if(inbox != null) {
			inboxCount = inbox.size();
		}
		sent = (ArrayList<Email>)i.getSerializableExtra("sent");
		allEmails = new ArrayList<Email>();
		if(inbox != null) {
			allEmails.addAll(inbox);
		}
		if(sent != null) {
			allEmails.addAll(sent);
		}
		
		sentCalls = (ArrayList<Call>)i.getSerializableExtra("sent_calls");
		receivedCalls = (ArrayList<Call>)i.getSerializableExtra("received_calls");
		allCalls = new ArrayList<Call>();
		if(sentCalls != null) {
			allCalls.addAll(sentCalls);
		}
		if(receivedCalls != null) {
			allCalls.addAll(receivedCalls);
		}
		
		setContentView(R.layout.activity_call_email_logs);
		logListView = (ListView) findViewById(R.id.callEmailList);
		logListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
		emailsButton = (Button) findViewById(R.id.emailsBtn);
		callsButton = (Button) findViewById(R.id.callsBtn);
		createButton = (Button) findViewById(R.id.createLogsBtn);
		
		unselected = emailsButton.getTextColors().getDefaultColor();
		selected = Color.rgb(0, 122, 255);
		
		logListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(final AdapterView<?> list, final View v,
			    final int position, final long id) {
				if(listEnum == 1) {
					if(!((Email)logListView.getItemAtPosition(position)).isChecked()) {
						((Email)logListView.getItemAtPosition(position)).setChecked(true);
						selectedEmails.add((Email)logListView.getItemAtPosition(position));
						updateView(position, true);
					} else {
						((Email)logListView.getItemAtPosition(position)).setChecked(false);
						selectedEmails.remove((Email)logListView.getItemAtPosition(position));
						updateView(position, false);
					}
					if(((EmailAdapter)logListView.getAdapter()).numItemsChecked() > 0) {
						createButton.setVisibility(View.VISIBLE);
					} else {
						createButton.setVisibility(View.GONE);
					}
				} else {
					if(!((Call)logListView.getItemAtPosition(position)).isChecked()) {
						((Call)logListView.getItemAtPosition(position)).setChecked(true);
						selectedCalls.add((Call)logListView.getItemAtPosition(position));
						updateView(position, true);
					} else {
						((Call)logListView.getItemAtPosition(position)).setChecked(false);
						selectedCalls.remove((Call)logListView.getItemAtPosition(position));
						updateView(position, false);
					}
					if(((CallsAdapter)logListView.getAdapter()).numItemsChecked() > 0) {
						createButton.setVisibility(View.VISIBLE);
					} else {
						createButton.setVisibility(View.GONE);
					}
				}
				
			  logListView.smoothScrollToPosition(position); // Make sure selection is plainly visible
			}
		});
		
		backBtn = (ImageView) findViewById(R.id.backBtnCallsEmails);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(-1);
				finish();
			}
			
		});
		
		if(allEmails.size() > 0)
			emailsClicked(null);
		else
			callsClicked(null);
		
	}
	
	private void updateView(int index, boolean check){
	    View v = logListView.getChildAt(index - 
	        logListView.getFirstVisiblePosition());

	    if(v == null)
	       return;

	    ImageView checkmark = (ImageView) v.findViewById(R.id.checkmark);
	    if(check)
	    	checkmark.setVisibility(View.VISIBLE);
	    else
	    	checkmark.setVisibility(View.GONE);
	}
	
	public void emailsClicked(View v) {
		selectedCalls.clear();
		selectedEmails.clear();
		createButton.setVisibility(View.GONE);
		listEnum = 1;
		emailsButton.setTextColor(selected);
		callsButton.setTextColor(unselected);
		adapter = new EmailAdapter(CallEmailLogActivity.this);
		adapter.addSectionHeaderItem("Inbox");
		for(int i1 = 0; i1 < allEmails.size(); i1++) {
			adapter.addItem(allEmails.get(i1));
			if(i1 == inbox.size()-1) {
				adapter.addSectionHeaderItem("Sent");
			}
		}
		
		logListView.setAdapter(adapter);
		
		if(listEnum == 2) {
			((CallsAdapter)logListView.getAdapter()).clearCheckedItems();
		} else {
			((EmailAdapter)logListView.getAdapter()).clearCheckedItems();
		}
		
	}
	
	public void submitClicked(View v) {
		
		if(listEnum == 1) {
			submittable = new JSONArray();
			for(int i = 0; i < selectedEmails.size(); i++) {
				JSONObject o = new JSONObject();
				Email e = selectedEmails.get(i);
				try{
					o.put("DateTime", e.getDate());
					o.put("displayName", e.getDisplayName() == null ? e.getEmail() : e.getDisplayName());
					o.put("subject", e.getSubject());
					submittable.put(o);
					
				}
				catch(JSONException ex) {
					
				}
			}
			//submit json array
			EntriesSubmissionTask est = new EntriesSubmissionTask();
			est.execute();
		} else if(listEnum == 2) {
			submittable = new JSONArray();
			for(int i = 0; i < selectedCalls.size(); i++) {
				JSONObject o = new JSONObject();
				Call c = selectedCalls.get(i);
				try{
					o.put("date", c.getDate());
					o.put("name", c.getPhoneNumber());
					o.put("length", getLength(c));
					//o.put("seconds", c.getSeconds());
					submittable.put(o);
				}
				catch(JSONException ex) {
					
				}
			}
			//submit json array
			EntriesSubmissionTask est = new EntriesSubmissionTask();
			est.execute();
		}
	}
	
	public void callsClicked(View v) {
		selectedCalls.clear();
		selectedEmails.clear();
		createButton.setVisibility(View.GONE);
		listEnum = 2;
		emailsButton.setTextColor(unselected);
		callsButton.setTextColor(selected);
		CallsAdapter callAdapter = new CallsAdapter(CallEmailLogActivity.this);
		callAdapter.addSectionHeaderItem("Calls Made");
		
		for(int i1 = 0; i1 < allCalls.size(); i1++) {
			callAdapter.addItem(allCalls.get(i1));
			if(i1 == sentCalls.size()-1) {
				callAdapter.addSectionHeaderItem("Calls Received");
			}
		}
		
		logListView.setAdapter(callAdapter);
		
		if(listEnum == 2) {
			((CallsAdapter)logListView.getAdapter()).clearCheckedItems();
		} else {
			((EmailAdapter)logListView.getAdapter()).clearCheckedItems();
		}
		
	}
	
	private class EntriesSubmissionTask extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(CallEmailLogActivity.this);
			progressDialog.setMessage("Creating entries...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

		
			if(listEnum == 1) {
				rqts = new RequestToServer("create_email_logs");
			} else if(listEnum == 2) {
				rqts = new RequestToServer("create_call_logs");
			}
			rqts.setLogsJSON(submittable);
			
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 1);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			if (sjta.getJsObjResponse() != null) {
				try {
					if(sjta.getJsObjResponse().has("session")) {
						Intent mIntent = new Intent(CallEmailLogActivity.this, LoginActivity.class);
						mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
						mIntent.putExtra("invalid_session", true);
						startActivity(mIntent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
						finish();
						return;
					} else if(sjta.getJsObjResponse().get("success") != null) {
						//success
						if(listEnum == 1) {
							selectedEmails.clear();
							
							for(int i = allEmails.size()-1; i >= 0; i--) {
								if(allEmails.get(i).isChecked()) {
									allEmails.remove(i);
								}
							}
							for(int i = inbox.size()-1; i >= 0; i--) {
								if(inbox.get(i).isChecked()) {
									inbox.remove(i);
								}
							}
							for(int i = sent.size()-1; i >= 0; i--) {
								if(sent.get(i).isChecked()) {
									sent.remove(i);
								}
							}
							adapter = new EmailAdapter(CallEmailLogActivity.this);
							adapter.addSectionHeaderItem("Inbox");
							for(int i1 = 0; i1 < allEmails.size(); i1++) {
								adapter.addItem(allEmails.get(i1));
								if(i1 == inbox.size()-1) {
									adapter.addSectionHeaderItem("Sent");
								}
							}
							createButton.setVisibility(View.GONE);
							((EmailAdapter)logListView.getAdapter()).clearCheckedItems();
							logListView.setAdapter(adapter);
						} else if(listEnum == 2) {
							selectedCalls.clear();
							
							for(int i = allCalls.size()-1; i >= 0; i--) {
								if(allCalls.get(i).isChecked()) {
									allCalls.remove(i);
								}
							}
							for(int i = sentCalls.size()-1; i >= 0; i--) {
								if(sentCalls.get(i).isChecked()) {
									sentCalls.remove(i);
								}
							}
							for(int i = receivedCalls.size()-1; i >= 0; i--) {
								if(receivedCalls.get(i).isChecked()) {
									receivedCalls.remove(i);
								}
							}
							((CallsAdapter)logListView.getAdapter()).clearCheckedItems();
							CallsAdapter callAdapter = new CallsAdapter(CallEmailLogActivity.this);
							callAdapter.addSectionHeaderItem("Calls Made");
							createButton.setVisibility(View.GONE);
							for(int i1 = 0; i1 < allCalls.size(); i1++) {
								callAdapter.addItem(allCalls.get(i1));
								if(i1 == sentCalls.size()-1) {
									callAdapter.addSectionHeaderItem("Received Calls");
								}
							}
							
							logListView.setAdapter(callAdapter);
						}
					}
				}catch(Exception e) {
					Log.e("zk", e.getLocalizedMessage());
				}

			}

			super.onPostExecute(result);
		}
	}
	
	private String getLength(Call c) {
		int min = c.getMinutes();
		int hr = c.getHours();
		double hours = hr;
		double minFraction;
		if(min < 9) {
			minFraction = .10;
		} else if(min <=13) {
			minFraction = .20;
		} else if(min <=16) {
			minFraction = .25;
		} else if(min < 21) {
			minFraction = .30;
		} else if(min < 27) {
			minFraction = .40;
		} else if(min < 33) {
			minFraction = .50;
		} else if(min < 39) {
			minFraction = .60;
		} else if(min < 43) {
			minFraction = .70;
		} else if(min < 46) {
			minFraction = .75;
		} else if(min < 51) {
			minFraction = .80;
		} else if(min < 57) {
			minFraction = .90;
		} else { 
			minFraction = 1.00;
		}
		
		double ret = hours+minFraction;
		
		return String.valueOf(ret);
		
	}
}
