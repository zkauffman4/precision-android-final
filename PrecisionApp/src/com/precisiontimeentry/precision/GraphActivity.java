package com.precisiontimeentry.precision;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.precisiontimeentry.precision.model.LoginData;
import com.precisiontimeentry.precision.model.Matter;
import com.precisiontimeentry.precision.model.NameValuePairAdapter;
import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

import com.androidplot.ui.SeriesRenderer;
import com.androidplot.ui.widget.TextLabelWidget;
import com.androidplot.xy.*;

public class GraphActivity extends Activity {

	private ProgressDialog progressDialog;
	private Number[] y_vals;
	private ListView resultsList;
	private int type;	//1 = hours, or 2 = entries, or 3 = matters
	private static String period;
	private XYPlot plot;
	private MyBarFormatter barFormatter;
	private XYSeries myXYSeries;
	private String valueLabel;
	private String domainLabel;
	private String titleBarLabel;
	private String[] dateLabels;
	private ImageView backBtn;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_graph);
		backBtn = (ImageView) findViewById(R.id.backBtnGraph);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(-1);
				finish();
			}
			
		});
		Intent i = getIntent();
		type = i.getIntExtra("type", 1);
		period = i.getStringExtra("period");
		plot = (XYPlot) findViewById(R.id.mySimpleXYPlot);
		barFormatter = new MyBarFormatter(Color.argb(255, 77, 187, 95), Color.LTGRAY);
		plot.setRangeLabel("");
		//Paint backgroundPaint = new Paint();
		//backgroundPaint.setColor(Color.WHITE);
		//configure the graph
		plot.getBackgroundPaint().setColor(Color.WHITE);
		plot.getGraphWidget().getBackgroundPaint().setColor(Color.WHITE);
		plot.getGraphWidget().getGridBackgroundPaint().setColor(Color.WHITE);
		plot.setTicksPerRangeLabel(1);
        plot.setRangeLowerBoundary(0, BoundaryMode.FIXED);
        plot.getGraphWidget().setGridPadding(60, 10, 60, 10);
        plot.setTicksPerDomainLabel(1);
        plot.getGraphWidget().getDomainGridLinePaint().setColor(Color.WHITE);
        plot.setDomainStep(XYStepMode.INCREMENT_BY_VAL, 1);
        
        
        
        if(type == 1) {
        	titleBarLabel = "Hours by ";
        	valueLabel = "Hours";
        } else if(type == 2) {
        	titleBarLabel = "Entries by ";
        	valueLabel = "Entries";
        } else if(type == 3) {
        	titleBarLabel = "Matters by ";
        	valueLabel = "Matters";
        }
        
        if(period.equals("day")) {
        	titleBarLabel += "Day";
        	domainLabel = "Day";
        } else if(period.equals("week")) {
        	titleBarLabel += "Week";
        	domainLabel = "Week Of";
        } else if(period.equals("month")) {
        	titleBarLabel += "Month";
        	domainLabel = "Month";
        }
        TextView title = (TextView) findViewById(R.id.headerLabel);
        title.setText(titleBarLabel);
        plot.setDomainLabel(domainLabel);
        
        
        plot.setDomainValueFormat(new NumberFormat() {

			@Override
			public StringBuffer format(double arg0, StringBuffer arg1,
					FieldPosition arg2) {
				if(period.equals("day")) {
					return dayOfWeek((int)arg0);
				} else if(period.equals("month")) {
					return monthOfYear((int)arg0);
				} else if(period.equals("week")) {
					if(dateLabels != null) {
						int index = (int)arg0;
						return new StringBuffer(dateLabels[index]);
					}
				}
					
				return new StringBuffer("Unknown");
			}

			@Override
			public StringBuffer format(long arg0, StringBuffer arg1,
					FieldPosition arg2) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Number parse(String arg0, ParsePosition arg1) {
				// TODO Auto-generated method stub
				return null;
			}
        	
        });
        
        plot.setRangeValueFormat(new DecimalFormat("#"));

		GraphTask m = new GraphTask();
		m.execute();
		
	}
	
	//This takes an int between 0 and 6 and returns the corresponding day of week
	private StringBuffer dayOfWeek(int dayNum) {
		if(dayNum == 0) {
			return new StringBuffer("Sun");
		} else if(dayNum == 1) {
			return new StringBuffer("Mon");
		} else if(dayNum == 2) {
			return new StringBuffer("Tues");
		} else if(dayNum == 3) {
			return new StringBuffer("Wed");
		} else if(dayNum == 4) {
			return new StringBuffer("Thurs");
		} else if(dayNum == 5) {
			return new StringBuffer("Fri");
		} else if(dayNum == 6) {
			return new StringBuffer("Sat");
		}
		return new StringBuffer("unknown");
	}
	
	//This takes an int between 0 and 6 and returns the corresponding day of week
		private StringBuffer monthOfYear(int monthNum) {
			if(monthNum == 0) {
				return new StringBuffer("Jan");
			} else if(monthNum == 1) {
				return new StringBuffer("Feb");
			} else if(monthNum == 2) {
				return new StringBuffer("Mar");
			} else if(monthNum == 3) {
				return new StringBuffer("Apr");
			} else if(monthNum == 4) {
				return new StringBuffer("May");
			} else if(monthNum == 5) {
				return new StringBuffer("Jun");
			} else if(monthNum == 6) {
				return new StringBuffer("Jul");
			} else if(monthNum == 7) {
				return new StringBuffer("Aug");
			} else if(monthNum == 8) {
				return new StringBuffer("Sept");
			} else if(monthNum == 9) {
				return new StringBuffer("Oct");
			} else if(monthNum == 10) {
				return new StringBuffer("Nov");
			} else if(monthNum == 11) {
				return new StringBuffer("Dec");
			}
			return new StringBuffer("unknown");
		}
	
	private class GraphTask extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(GraphActivity.this);
			progressDialog.setMessage("Retrieving graph data...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			rqts = new RequestToServer("get_graph_data");
			if(type == 1)
				rqts.setTimePeriodAndType(period, "hours");
			else if(type == 2)
				rqts.setTimePeriodAndType(period, "entries");
			else 
				rqts.setTimePeriodAndType(period, "matters");
						
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 0);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			if (sjta.getJsObjResponse() != null) {
				try {
					if(sjta.getJsObjResponse().has("session")) {
						Intent mIntent = new Intent(GraphActivity.this, LoginActivity.class);
						mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
						mIntent.putExtra("invalid_session", true);
						startActivity(mIntent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
						finish();
						return;
					} else {
						JSONObject data = sjta.getJsObjResponse();
	
						if(period.equals("day")) {
							try {
								y_vals = getDaysFromJson(data);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							plot.setDomainLabel("Day");
						} else if(period.equals("week")) {
							JSONArray hours = data.getJSONArray("hours");
							dateLabels = new String[hours.length()];
							y_vals = new Number[hours.length()];
							for(int i = 0; i < hours.length(); i++) {
								JSONObject o = hours.getJSONObject(i);
								int index = o.getInt("index");
								String prev_sunday = o.getString("prev_sunday");
								prev_sunday = prev_sunday.substring(5);
								double count = o.getDouble("count");
								dateLabels[index] = prev_sunday;
								y_vals[index] = count;
							}
							plot.setDomainLabel("Week Of");
						} else if(period.equals("month")) {
							try {
								y_vals = getMonthFromJson(data);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							plot.setDomainLabel("Month");
						}
					}

				} catch (Exception e) {
					Toast.makeText(GraphActivity.this, "Could not retrieve matters, please try again", Toast.LENGTH_SHORT)
							.show();
					Log.e("", "Matter search failed !");
				}

			}

			super.onPostExecute(result);
			List<Number> l = Arrays.asList(y_vals);
				myXYSeries = new SimpleXYSeries(l, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, valueLabel);
				plot.addSeries(myXYSeries, barFormatter);
				
				double max = 0;
				for(int i = 0; i < l.size(); i++) {
					if(l.get(i).doubleValue() > max) {
						max = l.get(i).doubleValue();
					}
				}
				int multiplier = 1;
				int mult = 1;
				int numYLabels = 1;
				int newMaxY = 1;
				
				if(max < 50) {
					mult = (int)max/10 + 1;
					numYLabels = (int)max/mult + 1;
					newMaxY = numYLabels * mult;
				} else if(max < 500) {
					multiplier = (int)max/100 + 1;
					mult = multiplier * 10;
					numYLabels = (int)max/mult + 1;
					newMaxY = numYLabels * mult;
				} else {
					multiplier = (int)max/1000 + 1;
					mult = multiplier * 100;
					numYLabels = (int)max/mult + 1;
					newMaxY = numYLabels * mult;
				}
				
				plot.setRangeStep(XYStepMode.INCREMENT_BY_VAL, mult);
				plot.setRangeUpperBoundary((Number)newMaxY, BoundaryMode.FIXED);
				plot.setBackgroundColor(Color.WHITE);
				MyBarRenderer renderer = ((MyBarRenderer)plot.getRenderer(MyBarRenderer.class));
				renderer.setBarWidthStyle(BarRenderer.BarWidthStyle.FIXED_WIDTH);
				
				plot.getGraphWidget().getRangeLabelPaint().setColor(Color.BLACK);
				plot.getGraphWidget().getDomainLabelPaint().setColor(Color.BLACK);
				
				if(!period.equals("month")) {
					plot.getGraphWidget().setRangeLabelHorizontalOffset(30);
					renderer.setBarWidth(80);
				} else {
					plot.getGraphWidget().setRangeLabelHorizontalOffset(18);
					renderer.setBarWidth(60);
				}
				plot.getLegendWidget().setVisible(false);
		        plot.redraw();
		}
		
		private Number[] getDaysFromJson(JSONObject data) throws Exception {
			try{
				Number[] y_vals = new Number[7];
				y_vals[0] = data.getDouble("Sun");
				y_vals[1] = data.getDouble("Mon");
				y_vals[2] = data.getDouble("Tues");
				y_vals[3] = data.getDouble("Wed");
				y_vals[4] = data.getDouble("Thurs");
				y_vals[5] = data.getDouble("Fri");
				y_vals[6] = data.getDouble("Sat");
				return y_vals;
			}catch(Exception e) {
				throw e;
			}
			
		}
		
		private Number[] getMonthFromJson(JSONObject data) throws Exception {
			try{
				Number[] y_vals = new Number[12];
				y_vals[0] = data.getDouble("Jan");
				y_vals[1] = data.getDouble("Feb");
				y_vals[2] = data.getDouble("Mar");
				y_vals[3] = data.getDouble("Apr");
				y_vals[4] = data.getDouble("May");
				y_vals[5] = data.getDouble("Jun");
				y_vals[6] = data.getDouble("Jul");
				y_vals[7] = data.getDouble("Aug");
				y_vals[8] = data.getDouble("Sept");
				y_vals[9] = data.getDouble("Oct");
				y_vals[10] = data.getDouble("Nov");
				y_vals[11] = data.getDouble("Dec");
				return y_vals;
			}catch(Exception e) {
				throw e;
			}
			
		}
		
	}
	
}

class MyBarFormatter extends BarFormatter {
    public MyBarFormatter(int fillColor, int borderColor) {
        super(fillColor, borderColor);
    }

    @Override
    public Class<? extends SeriesRenderer> getRendererClass() {
        return MyBarRenderer.class;
    }

    @Override
    public SeriesRenderer getRendererInstance(XYPlot plot) {
        return new MyBarRenderer(plot);
    }
}

class MyBarRenderer extends BarRenderer<MyBarFormatter> {

    public MyBarRenderer(XYPlot plot) {
        super(plot);
    }

    /**
     * Implementing this method to allow us to inject our
     * special selection formatter.
     * @param index index of the point being rendered.
     * @param series XYSeries to which the point being rendered belongs.
     * @return
     */
    @Override
    public MyBarFormatter getFormatter(int index, XYSeries series) { 
        
            return getFormatter(series);
    }
}
