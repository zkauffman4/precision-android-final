package com.precisiontimeentry.precision;

import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.precisiontimeentry.precision.model.Entry;
import com.precisiontimeentry.precision.model.EntryAdapter;
import com.precisiontimeentry.precision.model.Matter;
import com.precisiontimeentry.precision.model.NameValuePairAdapter;
import com.precisiontimeentry.precision.model.ObscuredSharedPreferences;
import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class StatsActivity extends Activity implements OnClickListener {

	private Button weekButton, monthButton, yearButton;
	FragmentManager mFragmentManager;
	FragmentTransaction mFragmentTransaction;
	Fragment mFragment;
	private int tabEnum;
	private ProgressDialog progressDialog;
	private ArrayList<Entry> entriesArray;
	private ArrayList<Matter> mattersArray;
	private TextView hoursGraphLabel, mattersGraphLabel, entriesGraphLabel, progressLabel, hoursByMatterLabel, entriesByMatterLabel, hoursLabel;
	private TextView entriesLabel, mattersLabel;
	private ImageView hoursGraphImage, hoursByMatterGraphImage, entriesGraphImage, entriesByMatterGraphImage, mattersGraphImage;
	private ProgressBar goalProgressBar;
	private TextView headerLabel;
	private int selected, unselected;
	private int weeklyGoal;
	private ObscuredSharedPreferences preferences;
	private double total_hours;
	private ImageView backBtn;
	
	//@SuppressLint("NewApi")
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_time_stats);
		
		progressDialog = new ProgressDialog(StatsActivity.this);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setCancelable(true);
		
		backBtn = (ImageView) findViewById(R.id.backBtnStats);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(-1);
				finish();
			}
			
		});
		
		tabEnum = 1;
		hoursGraphLabel = (TextView) findViewById(R.id.hoursGraphLabel);
		mattersGraphLabel = (TextView) findViewById(R.id.mattersGraphLabel);
		entriesGraphLabel = (TextView) findViewById(R.id.entriesGraphLabel);
		progressLabel = (TextView) findViewById(R.id.goalProgressLabel);
		hoursByMatterLabel = (TextView) findViewById(R.id.hoursByMatterLabel);
		entriesByMatterLabel = (TextView) findViewById(R.id.entriesByMatterGraphLabel);
		hoursLabel = (TextView) findViewById(R.id.hoursLabel);
		entriesLabel = (TextView) findViewById(R.id.entriesLabel);
		mattersLabel = (TextView) findViewById(R.id.mattersLabel);
		
		hoursGraphImage = (ImageView) findViewById(R.id.hoursGraphImage);
		hoursByMatterGraphImage = (ImageView) findViewById(R.id.hoursByMatterImage);
		entriesGraphImage = (ImageView) findViewById(R.id.entriesGraphImage);
		entriesByMatterGraphImage = (ImageView) findViewById(R.id.entriesByMatterGraphImage);
		mattersGraphImage = (ImageView) findViewById(R.id.mattersGraphImage);
		
		goalProgressBar = (ProgressBar) findViewById(R.id.goalProgressBar);
		headerLabel = (TextView) findViewById(R.id.statsHeaderLabel);
		
		
		View.OnClickListener hoursGraphPressed = new View.OnClickListener() {
		    public void onClick(View v) {
		    	Intent mIntent = new Intent(StatsActivity.this, GraphActivity.class);
		    	mIntent.putExtra("type", 1);
		      if(tabEnum == 1) {
			    	mIntent.putExtra("period", "day");
		      } else if(tabEnum == 2) {
			    	mIntent.putExtra("period", "week");
		      } else if(tabEnum == 3) {
			    	mIntent.putExtra("period", "month");
		      }
		      startActivity(mIntent);
		      
		    }
		  };
		  View.OnClickListener hoursByMatterPressed = new View.OnClickListener() {
		    public void onClick(View v) {
		    	Intent mIntent = new Intent(StatsActivity.this, ByMatterStatsActivity.class);
		    	mIntent.putExtra("type", 1);
		    	if(tabEnum == 1) {
			    	  mIntent.putExtra("period", "week");
		      	} else if(tabEnum == 2) {
		      		 mIntent.putExtra("period", "month");
		      	} else if(tabEnum == 3) {
		      		 mIntent.putExtra("period", "year");
		        }
		    	startActivity(mIntent);
		    }
		  };
		  View.OnClickListener entriesGraphPressed = new View.OnClickListener() {
		    public void onClick(View v) {
		    	Intent mIntent = new Intent(StatsActivity.this, GraphActivity.class);
		    	mIntent.putExtra("type", 2);
		    	if(tabEnum == 1) {
		    		mIntent.putExtra("period", "day");
				} else if(tabEnum == 2) {
					mIntent.putExtra("period", "week");
				} else if(tabEnum == 3) {
					mIntent.putExtra("period", "month");
				}
		    	startActivity(mIntent);
		    }
		  };
		  View.OnClickListener entriesByMatterPressed = new View.OnClickListener() {
			    public void onClick(View v) {
			    	Intent mIntent = new Intent(StatsActivity.this, ByMatterStatsActivity.class);
			    	mIntent.putExtra("type", 2);
			    	if(tabEnum == 1) {
				    	  mIntent.putExtra("period", "week");
			      	} else if(tabEnum == 2) {
			      		 mIntent.putExtra("period", "month");
			      	} else if(tabEnum == 3) {
			      		 mIntent.putExtra("period", "year");
			        }
			    	startActivity(mIntent);
			    }
			    
		  };
		  View.OnClickListener mattersGraphPressed = new View.OnClickListener() {
		    public void onClick(View v) {
		    	Intent mIntent = new Intent(StatsActivity.this, GraphActivity.class);
		    	mIntent.putExtra("type", 3);
		    	if(tabEnum == 1) {
		    		mIntent.putExtra("period", "day");
		    	} else if(tabEnum == 2) {
		    		mIntent.putExtra("period", "week");
		    	} else if(tabEnum == 3) {
		    		mIntent.putExtra("period", "month");
		        }
		    	startActivity(mIntent);
		    }
		  };
		  
		  hoursGraphLabel.setOnClickListener(hoursGraphPressed);
		  hoursGraphImage.setOnClickListener(hoursGraphPressed);
		  mattersGraphLabel.setOnClickListener(mattersGraphPressed);
		  mattersGraphImage.setOnClickListener(mattersGraphPressed);
		  hoursByMatterGraphImage.setOnClickListener(hoursByMatterPressed);
		  hoursByMatterLabel.setOnClickListener(hoursByMatterPressed);
		  entriesByMatterLabel.setOnClickListener(entriesByMatterPressed);
		  entriesByMatterGraphImage.setOnClickListener(entriesByMatterPressed);
		  entriesGraphLabel.setOnClickListener(entriesGraphPressed);
		  entriesGraphImage.setOnClickListener(entriesGraphPressed);
		  
		 preferences = new ObscuredSharedPreferences(this, this.getSharedPreferences("Precision Preferences", Context.MODE_PRIVATE)); 
		if(!preferences.getBoolean("enableWeeklyTracking", false)) {
			LinearLayout l = (LinearLayout) findViewById(R.id.weeklyGoalProgress);
			l.setVisibility(View.GONE);
		} else {
			weeklyGoal = preferences.getInt("weekly_goal_value", 40);
		}
		
		entriesArray = new ArrayList<Entry>();
		mattersArray = new ArrayList<Matter>();
		weekButton = (Button) findViewById(R.id.btn_week_stats);
		monthButton = (Button) findViewById(R.id.btn_month_stats);
		yearButton = (Button) findViewById(R.id.btn_year_stats);
		
		unselected = monthButton.getTextColors().getDefaultColor();
		selected = Color.rgb(0, 122, 255);
		
		weekButton.setTextColor(selected);

		weekButton.setOnClickListener(this);
		monthButton.setOnClickListener(this);
		yearButton.setOnClickListener(this);
		
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;
		
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		float ydpi = metrics.ydpi;
		int new_height = (int) ((int)height/metrics.density);
		
		LinearLayout l = (LinearLayout)findViewById(R.id.margin1);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, height/50);
		l.setLayoutParams(lp);
		
		LinearLayout l2 = (LinearLayout)findViewById(R.id.margin2);
		LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, height/50);
		l2.setLayoutParams(lp2);
		
		LinearLayout l3 = (LinearLayout)findViewById(R.id.margin3);
		LinearLayout.LayoutParams lp3 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, height/50);
		l3.setLayoutParams(lp3);
		
		LinearLayout l4 = (LinearLayout)findViewById(R.id.margin4);
		LinearLayout.LayoutParams lp4 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, height/50);
		l4.setLayoutParams(lp4);
		
		LinearLayout l5 = (LinearLayout)findViewById(R.id.margin5);
		LinearLayout.LayoutParams lp5 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, height/50);
		l5.setLayoutParams(lp5);
		
		
		GetData task = new GetData();
		task.execute();
	}

	@Override
	public void onClick(View v) {
		entriesArray.clear();
		mattersArray.clear();
		GetData task = new GetData();
		switch (v.getId()) {
		case R.id.btn_week_stats:
			tabEnum = 1;
			weekButton.setTextColor(selected);
			monthButton.setTextColor(unselected);
			yearButton.setTextColor(unselected);
			headerLabel.setText("This Week");
			break;
		case R.id.btn_month_stats:
			tabEnum = 2;
			weekButton.setTextColor(unselected);
			monthButton.setTextColor(selected);
			yearButton.setTextColor(unselected);
			headerLabel.setText("This Month");
			break;
		case R.id.btn_year_stats:
			tabEnum = 3;
			weekButton.setTextColor(unselected);
			monthButton.setTextColor(unselected);
			yearButton.setTextColor(selected);
			headerLabel.setText("This Year");
			break;

		default:
			break;
		}
		task.execute();
	}
	
	private class GetData extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog.show();
			if(tabEnum == 1) {
				hoursGraphLabel.setText("Hours by day");
				mattersGraphLabel.setText("Matters by day");
				entriesGraphLabel.setText("Entries by day");
				hoursByMatterLabel.setText("Hours by matter");
				entriesByMatterLabel.setText("Entries by matter");
			} else if(tabEnum == 2) {
				hoursGraphLabel.setText("Hours by week");
				mattersGraphLabel.setText("Matters by week");
				entriesGraphLabel.setText("Entries by week");
				hoursByMatterLabel.setText("Hours by matter");
				entriesByMatterLabel.setText("Entries by matter");
			} else if(tabEnum == 3) {
				hoursGraphLabel.setText("Hours by month");
				mattersGraphLabel.setText("Matters by month");
				entriesGraphLabel.setText("Entries by month");
				hoursByMatterLabel.setText("Hours by matter");
				entriesByMatterLabel.setText("Entries by matter");
			}
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

		
			if(tabEnum == 1) {
				rqts = new RequestToServer("stats_this_week");
			} else if(tabEnum == 2) {
				rqts = new RequestToServer("stats_this_month");
			} else if(tabEnum == 3) {
				rqts = new RequestToServer("stats_this_year");
			}
			
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 0);
			
			

			return null;
		}

		
		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			if (sjta.getJsObjResponse() != null) {
				if(sjta.getJsObjResponse().has("session")) {
					Intent mIntent = new Intent(StatsActivity.this, LoginActivity.class);
					mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					mIntent.putExtra("invalid_session", true);
					startActivity(mIntent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
					finish();
					return;
				} 
				if(tabEnum == 1) {
					try {
						
						JSONObject stats = sjta.getJsObjResponse().getJSONObject("stats");
						String total_hours1 = stats.getString("total_hours");
						total_hours = Double.parseDouble(total_hours1);
						String total_entries = stats.getString("total_entries");
						String total_matters = stats.getString("total_matters");
						
						hoursLabel.setText("Hours this week: "+total_hours);
						entriesLabel.setText("Entries this week: "+total_entries);
						mattersLabel.setText("Matters this week: "+total_matters);
						
	
					} catch (JSONException e) {
						Toast.makeText(StatsActivity.this, "Could not stat data, please try again", Toast.LENGTH_SHORT)
								.show();
						Log.e("", "Stat data request failed!");
					}
				} else if(tabEnum == 2) {
					try {
						
						JSONObject stats = sjta.getJsObjResponse().getJSONObject("stats");
						String total_hours1 = stats.getString("total_hours");
						total_hours = Double.parseDouble(total_hours1);
						String total_entries = stats.getString("total_entries");
						String total_matters = stats.getString("total_matters");
						
						hoursLabel.setText("Hours this month: "+total_hours);
						entriesLabel.setText("Entries this month: "+total_entries);
						mattersLabel.setText("Matters this month: "+total_matters);
	
					} catch (JSONException e) {
						Toast.makeText(StatsActivity.this, "Could not stat data, please try again", Toast.LENGTH_SHORT)
								.show();
						Log.e("", "Stat data request failed!");
					}
				} else if(tabEnum == 3) {
					try {
						
						JSONObject stats = sjta.getJsObjResponse().getJSONObject("stats");
						String total_hours1 = stats.getString("total_hours");
						total_hours = Double.parseDouble(total_hours1);
						String total_entries = stats.getString("total_entries");
						String total_matters = stats.getString("total_matters");
						
						hoursLabel.setText("Hours this year: "+total_hours1);
						entriesLabel.setText("Entries this year: "+total_entries);
						mattersLabel.setText("Matters this year: "+total_matters);
						
	
					} catch (JSONException e) {
						Toast.makeText(StatsActivity.this, "Could not stat data, please try again", Toast.LENGTH_SHORT)
								.show();
						Log.e("", "Stat data request failed!");
					}
				}

			}

			super.onPostExecute(result);
			
			if(!preferences.getBoolean("enableWeeklyTracking", false)) {
				LinearLayout l = (LinearLayout) findViewById(R.id.weeklyGoalProgress);
				l.setVisibility(View.GONE);
			} else {
				int weeklyGoal = preferences.getInt("weekly_goal_value", 40);
				if(tabEnum == 1) {
					goalProgressBar.setMax(weeklyGoal);
					goalProgressBar.setProgress((int)total_hours);
					int t = (int)(total_hours/weeklyGoal * 100);
					progressLabel.setText("Weekly progress: "+t+"%");
				} else if(tabEnum == 2) {
					Calendar cal = Calendar.getInstance();
					int numDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
					double mult = (double)numDays/7;
					goalProgressBar.setMax((int)(weeklyGoal*mult));
					goalProgressBar.setProgress((int)total_hours);
					int t = (int)(total_hours/(weeklyGoal*mult) * 100);
					progressLabel.setText("Monthly progress: "+t+"%");
				} else {
					goalProgressBar.setMax(weeklyGoal*48);
					goalProgressBar.setProgress((int)total_hours);
					int t = (int)(total_hours/(weeklyGoal*48) * 100);
					progressLabel.setText("Yearly progress: "+t+"%");
				}
			}
		}
	}

}
