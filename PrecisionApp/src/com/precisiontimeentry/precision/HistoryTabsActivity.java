package com.precisiontimeentry.precision;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.precisiontimeentry.precision.model.Entry;
import com.precisiontimeentry.precision.model.EntryAdapter;
import com.precisiontimeentry.precision.model.HistoryAdapter;
import com.precisiontimeentry.precision.model.ItemAdapter;
import com.precisiontimeentry.precision.model.ItemAdapterMatterHistory;
import com.precisiontimeentry.precision.model.Matter;
import com.precisiontimeentry.precision.model.NameValuePairAdapter;
import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class HistoryTabsActivity extends Activity implements OnClickListener {

	private Button daysButton, monthButton, matterButton;
	FragmentManager mFragmentManager;
	FragmentTransaction mFragmentTransaction;
	Fragment mFragment;
	private int tabEnum;
	private ProgressDialog progressDialog;
	private ArrayList<Entry> entriesArray;
	private ArrayList<Matter> mattersArray;
	private SwipeListView historyListView;
	private ImageView backBtn;
	private boolean isListItemOpen;
	private Activity activity;
	private int selected, unselected, mPositionOfOpened;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history_tabs);
		activity = this;
		entriesArray = new ArrayList<Entry>();
		mattersArray = new ArrayList<Matter>();
		daysButton = (Button) findViewById(R.id.btn_days);
		monthButton = (Button) findViewById(R.id.btn_months);
		matterButton = (Button) findViewById(R.id.btn_matter);
		
		unselected = daysButton.getTextColors().getDefaultColor();
		selected = Color.rgb(0, 122, 255);
		daysButton.setTextColor(selected);

		daysButton.setOnClickListener(this);
		monthButton.setOnClickListener(this);
		matterButton.setOnClickListener(this);
		
		backBtn = (ImageView) findViewById(R.id.backBtnHistoryTabs);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(-1);
				finish();
			}
			
		});
		
		historyListView = (SwipeListView) findViewById(R.id.historyListView);
		
		historyListView.setSwipeListViewListener(new BaseSwipeListViewListener() {
	         @Override
	         public void onOpened(int position, boolean toRight) {
	        	 if(tabEnum != 3) {
		        	 entriesArray.get(position).setOpenedToLeft(!toRight);
		             entriesArray.get(position).setOpenedToRight(toRight);
		        	 if(mPositionOfOpened != -1 && mPositionOfOpened < entriesArray.size() && mPositionOfOpened != position) {
		        		 historyListView.closeAnimate(mPositionOfOpened);
		        	 }
	        	 }
	        	 mPositionOfOpened = position;
	        	 
	         }
	 
	         @Override
	         public void onClosed(int position, boolean fromRight) {
	        	 isListItemOpen = false;
	         }
	 
	         @Override
	         public void onListChanged() {
	         }
	 
	         @Override
	         public void onMove(int position, float x) {
	         }
	 
	         @Override
	         public void onStartOpen(int position, int action, boolean right) {
	        
	             entriesArray.get(position).setOpenedToLeft(!right);
	             entriesArray.get(position).setOpenedToRight(right);
	         }
	 
	         @Override
	         public void onStartClose(int position, boolean right) {
	             Log.d("swipe", String.format("onStartClose %d", position));
	         }
	 
	         @Override
	         public void onClickFrontView(int position) {
	             Log.d("swipe", String.format("onClickFrontView %d", position));
	 
	             historyListView.openAnimate(position); //when you touch front view it will open
	 
	         }
	 
	         @Override
	         public void onClickBackView(int position) {
	             Log.d("swipe", String.format("onClickBackView %d", position));
	 
	             historyListView.closeAnimate(position);//when you touch back view it will close
	         }
	 
	         @Override
	         public void onDismiss(int[] reverseSortedPositions) {
	 
	         }
	 
	     });
		
		Display display = getWindowManager().getDefaultDisplay();
	    DisplayMetrics outMetrics = new DisplayMetrics ();
	    display.getMetrics(outMetrics);

	    float density  = getResources().getDisplayMetrics().density;
	    float dpHeight = outMetrics.heightPixels / density;
	    float dpWidth  = outMetrics.widthPixels / density;
		
		historyListView.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT); // there are five swiping modes
		historyListView.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL); //there are four swipe actions
		//historyListView.setOffsetLeft(convertDpToPixel(dpWidth*.33f)); // left side offset
		historyListView.setOffsetRight(convertDpToPixel(dpWidth*.33f)); // right side offset
		historyListView.setAnimationTime(50); // animation time
		historyListView.setSwipeOpenOnLongPress(false); // enable or disable SwipeOpenOnLongPress

		tabEnum = 1;
		GetHistory task = new GetHistory();
		task.execute();
		historyListView.setOnItemClickListener(new OnItemClickListener() {
	         @Override
	         public void onItemClick(AdapterView<?> a, View v, int position, long id) { 
	        	 if(tabEnum == 3) {
		        	 Object o = historyListView.getItemAtPosition(position);
		        	 Matter fullObject = (Matter)o;
		        	 String matterId = fullObject.getMatterId();
		        	 Intent i = new Intent(getApplicationContext(), MatterHistoryActivity.class);
		        	 i.putExtra("matter_id", matterId);
		        	 startActivity(i);
	        	 }
	         }

			
	        });
	}
	
	public void cloneEntry(int arraySpot) {
		Entry e = entriesArray.get(arraySpot);
		Intent i = new Intent(HistoryTabsActivity.this, NewEntryActivity.class);
		i.putExtra("cloningEntry", e);
		startActivityForResult(i, 203);
	}
	
	public int convertDpToPixel(float dp) {
	       DisplayMetrics metrics = getResources().getDisplayMetrics();
	       float px = dp * (metrics.densityDpi / 160f);
	       return (int) px;
	   }

	@Override
	public void onClick(View v) {
		entriesArray.clear();
		mattersArray.clear();
		GetHistory task = new GetHistory();
		switch (v.getId()) {
		case R.id.btn_days:
			daysButton.setTextColor(selected);
			monthButton.setTextColor(unselected);
			matterButton.setTextColor(unselected);
			tabEnum = 1;
			break;
		case R.id.btn_months:
			daysButton.setTextColor(unselected);
			monthButton.setTextColor(selected);
			matterButton.setTextColor(unselected);
			tabEnum = 2;
			break;
		case R.id.btn_matter:
			daysButton.setTextColor(unselected);
			monthButton.setTextColor(unselected);
			matterButton.setTextColor(selected);
			tabEnum = 3;
			break;

		default:
			break;
		}
		task.execute();
	}
	
	private class GetHistory extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(HistoryTabsActivity.this);
			progressDialog.setMessage("Retrieving history...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

		
			if(tabEnum == 1) {
				rqts = new RequestToServer("history_last_7");
			} else if(tabEnum == 2) {
				rqts = new RequestToServer("history_last_30");
			} else if(tabEnum == 3) {
				rqts = new RequestToServer("history_by_matter");
			}
			
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1002, 0);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();
			if(sjta.getJsObjResponse() != null) {
				if(sjta.getJsObjResponse().has("session")) {
					Intent mIntent = new Intent(HistoryTabsActivity.this, LoginActivity.class);
					mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					mIntent.putExtra("invalid_session", true);
					startActivity(mIntent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
					finish();
					return;
				} 
			}

			if (sjta.getJsMassResponse() != null) {
				if(tabEnum != 3) {
					try {
						
						JSONArray entries = sjta.getJsMassResponse();
						for(int i = 0; i < entries.length(); i++) {
							JSONObject o = entries.getJSONObject(i);
							String matterId = o.getString("matter_id");
							String matterName = o.getString("matter_name");
							String description = o.getString("description");
							String taskCode = o.getString("task_code_id");
							String componentCode = o.getString("component_code_id");
							String hours = o.getString("hours_worked");
							String date = o.getString("date_worked");
							String tGroup = o.getString("component_group");
							String cGroup = o.getString("task_group");
							Entry e = new Entry();
							e.setComponent_code_id(componentCode);
							e.setComponentGroup(cGroup);
							e.setDate(date);
							e.setDescription(description);
							e.setHours(hours);
							e.setMatterId(matterId);
							e.setName(matterName);
							e.setTask_code_id(taskCode);
							e.setTaskGroup(tGroup);
							
							entriesArray.add(e);
						}
	
					} catch (JSONException e) {
						Toast.makeText(HistoryTabsActivity.this, "Could not retrieve components, please try again", Toast.LENGTH_SHORT)
								.show();
						Log.e("", "Component search failed !");
					}
				} else {	//history by matter tab has different format
					JSONArray entries = sjta.getJsMassResponse();
					for(int i = 0; i < entries.length(); i++) {
						JSONObject o;
						try {
							o = entries.getJSONObject(i);
							String matterName = o.getString("matter_name");
							String matterId = o.getString("matter_id");
							Matter m = new Matter();
							m.setName(matterName);
							m.setMatterId(matterId);
							mattersArray.add(m);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}

			}

			super.onPostExecute(result);
			if(tabEnum != 3) {
				historyListView.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
				historyListView.setAdapter(new HistoryAdapter(HistoryTabsActivity.this, activity, R.layout.swipeable_row_clone, entriesArray));
			} else {
				historyListView.setSwipeMode(SwipeListView.SWIPE_MODE_NONE);
				historyListView.setAdapter(new ItemAdapterMatterHistory(HistoryTabsActivity.this, activity, R.layout.swipeable_row_nothing, mattersArray));
			}
		}
	}

}
