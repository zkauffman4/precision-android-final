package com.precisiontimeentry.precision;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.precisiontimeentry.precision.model.Entry;
import com.precisiontimeentry.precision.model.EntryAdapter;
import com.precisiontimeentry.precision.model.HistoryAdapter;
import com.precisiontimeentry.precision.model.LoginData;
import com.precisiontimeentry.precision.model.Matter;
import com.precisiontimeentry.precision.model.NameValuePairAdapter;
import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;

public class MatterHistoryActivity extends Activity {

	private ProgressDialog progressDialog;
	private ArrayList<Entry> entriesArray;
	private SwipeListView resultsList;
	private String selectedMatterId;
	private ImageView backBtn;
	private Activity activity;
	private int mPositionOfOpened;
	private boolean isListItemOpen;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = this;
		setContentView(R.layout.activity_matter_history);
		resultsList = (SwipeListView) findViewById(R.id.matterHistoryResults);
		entriesArray = new ArrayList<Entry>();
		
		backBtn = (ImageView) findViewById(R.id.backBtnMatterHistory);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(-1);
				finish();
			}
			
		});
		
		resultsList.setSwipeListViewListener(new BaseSwipeListViewListener() {
	         @Override
	         public void onOpened(int position, boolean toRight) {
		        	 entriesArray.get(position).setOpenedToLeft(!toRight);
		             entriesArray.get(position).setOpenedToRight(toRight);
		        	 if(mPositionOfOpened != -1 && mPositionOfOpened < entriesArray.size() && mPositionOfOpened != position) {
		        		 resultsList.closeAnimate(mPositionOfOpened);
		        	 }
	        	 mPositionOfOpened = position;
	        	 
	         }
	 
	         @Override
	         public void onClosed(int position, boolean fromRight) {
	        	 isListItemOpen = false;
	         }
	 
	         @Override
	         public void onListChanged() {
	         }
	 
	         @Override
	         public void onMove(int position, float x) {
	         }
	 
	         @Override
	         public void onStartOpen(int position, int action, boolean right) {
	        
	             entriesArray.get(position).setOpenedToLeft(!right);
	             entriesArray.get(position).setOpenedToRight(right);
	         }
	 
	         @Override
	         public void onStartClose(int position, boolean right) {
	             Log.d("swipe", String.format("onStartClose %d", position));
	         }
	 
	         @Override
	         public void onClickFrontView(int position) {
	             Log.d("swipe", String.format("onClickFrontView %d", position));
	 
	             resultsList.openAnimate(position); //when you touch front view it will open
	 
	         }
	 
	         @Override
	         public void onClickBackView(int position) {
	             Log.d("swipe", String.format("onClickBackView %d", position));
	 
	             resultsList.closeAnimate(position);//when you touch back view it will close
	         }
	 
	         @Override
	         public void onDismiss(int[] reverseSortedPositions) {
	 
	         }
	 
	     });
		
		Display display = getWindowManager().getDefaultDisplay();
	    DisplayMetrics outMetrics = new DisplayMetrics ();
	    display.getMetrics(outMetrics);

	    float density  = getResources().getDisplayMetrics().density;
	    float dpHeight = outMetrics.heightPixels / density;
	    float dpWidth  = outMetrics.widthPixels / density;
		
	    resultsList.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT); // there are five swiping modes
	    resultsList.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL); //there are four swipe actions
		//historyListView.setOffsetLeft(convertDpToPixel(dpWidth*.33f)); // left side offset
	    resultsList.setOffsetRight(convertDpToPixel(dpWidth*.33f)); // right side offset
	    resultsList.setAnimationTime(50); // animation time
	    resultsList.setSwipeOpenOnLongPress(false); // enable or disable SwipeOpenOnLongPress
		
		Intent i = getIntent();
		selectedMatterId = i.getStringExtra("matter_id");
		
		MatterHistoryTask m = new MatterHistoryTask();
		m.execute();
		
	}
	
	public void cloneEntry(int arraySpot) {
		Entry e = entriesArray.get(arraySpot);
		Intent i = new Intent(MatterHistoryActivity.this, NewEntryActivity.class);
		i.putExtra("cloningEntry", e);
		startActivityForResult(i, 203);
	}
	
	public int convertDpToPixel(float dp) {
	       DisplayMetrics metrics = getResources().getDisplayMetrics();
	       float px = dp * (metrics.densityDpi / 160f);
	       return (int) px;
	   }
	
	private class MatterHistoryTask extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(MatterHistoryActivity.this);
			progressDialog.setMessage("Retrieving matter history...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

		

			rqts = new RequestToServer("matter_history_list");
			rqts.setMatterIdGet(selectedMatterId);
			
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 0);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			if (sjta.getJsObjResponse() != null) {
				try {
					if(sjta.getJsObjResponse().has("session")) {
						Intent mIntent = new Intent(MatterHistoryActivity.this, LoginActivity.class);
						mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
						mIntent.putExtra("invalid_session", true);
						startActivity(mIntent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
						finish();
						return;
					} else if (sjta.getJsObjResponse().get("entries") != null) {
						JSONArray entries = sjta.getJsObjResponse().getJSONArray("entries");
						for(int i = 0; i < entries.length(); i++) {
							JSONObject o = entries.getJSONObject(i);
							String matterId = o.getString("matter_id");
							String matterName = o.getString("matter_name");
							String description = o.getString("description");
							String taskCode = o.getString("task_code_id");
							String componentCode = o.getString("component_code_id");
							String hours = o.getString("hours_worked");
							String date = o.getString("date_worked");
							Entry e = new Entry();
							e.setComponentGroup(o.getString("component_group"));
							e.setTaskGroup(o.getString("task_group"));
							e.setComponent_code_id(componentCode);
							e.setDate(date);
							e.setDescription(description);
							e.setHours(hours);
							e.setMatterId(matterId);
							e.setName(matterName);
							e.setTask_code_id(taskCode);
							
							entriesArray.add(e);
						}

					}
				} catch (JSONException e) {
					Toast.makeText(MatterHistoryActivity.this, "Could not retrieve matters, please try again", Toast.LENGTH_SHORT)
							.show();
					Log.e("", "Matter search failed !");
				}

			}

			super.onPostExecute(result);

				resultsList.setAdapter(new HistoryAdapter(MatterHistoryActivity.this, activity, R.layout.swipeable_row_clone, entriesArray));
		}
	}
	
}
