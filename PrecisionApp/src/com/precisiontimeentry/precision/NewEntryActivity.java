package com.precisiontimeentry.precision;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.precisiontimeentry.precision.model.Entry;
import com.precisiontimeentry.precision.model.LoginData;
import com.precisiontimeentry.precision.model.Matter;
import com.precisiontimeentry.precision.model.ObscuredSharedPreferences;
import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class NewEntryActivity extends Activity
implements DatePickerDialog.OnDateSetListener, StopWatchFragment.OnCompleteListener {

	private EditText _dateTextfield, _descriptionTextfield;
	private Spinner hoursSpinner, minutesSpinner;
	private TextView _matterText, _taskText, _componentText, _matterDescText, _taskDescText, _compDescText;
	private ToggleButton _laterSwitch;
	private DatePickerDialog _datePickerDialog;
	private ProgressDialog progressDialog;
	private Matter _selectedMatter;
	private Entry _editingEntry, _cloningEntry;
	private Entry e;
	private String later, operation, te;
	private ImageView backBtn;
	private boolean shouldGoToIncomplete, dateInvalidSubmitAnyway;
	private ImageView stopWatchBtn;
	
	//vars to post
	private String _matterName, _matterId, _hours, _minutes, _date, _description, _taskCode, _componentCode, _taskGroup, _componentGroup, _later;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_entry);
		Calendar cal = Calendar.getInstance();
		dateInvalidSubmitAnyway = false;
		shouldGoToIncomplete = false;
		stopWatchBtn = (ImageView) findViewById(R.id.stopWatchBtn);
		stopWatchBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				StopWatchFragment stop = new StopWatchFragment();
				stop.show(getFragmentManager(), "stopwatch_fragment");
			}
			
		});
		backBtn = (ImageView) findViewById(R.id.backBtnNewEntry);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(-1);
				finish();
			}
			
		});

		_laterSwitch = (ToggleButton) findViewById(R.id.laterSwitch);
		_dateTextfield = (EditText) findViewById(R.id.dateTextfield);
		hoursSpinner = (Spinner) findViewById(R.id.hours);
		minutesSpinner = (Spinner) findViewById(R.id.minutes);
		_descriptionTextfield = (EditText) findViewById(R.id.description);
		_matterText = (TextView) findViewById(R.id.matterSearchSubText);
		_matterDescText = (TextView) findViewById(R.id.matterSearchText);
		_taskText = (TextView) findViewById(R.id.taskCodeId);
		_componentText = (TextView) findViewById(R.id.componentCodeId);
		_taskDescText = (TextView) findViewById(R.id.taskCodeName);
		_compDescText = (TextView) findViewById(R.id.componentCodeName);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.hours_array, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		hoursSpinner.setAdapter(adapter);
		
		ArrayAdapter<CharSequence> minAdapter = ArrayAdapter.createFromResource(this,
		        R.array.minutes_array, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		minAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		minutesSpinner.setAdapter(minAdapter);
		
		_datePickerDialog = new DatePickerDialog(this, this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
		_dateTextfield.setOnFocusChangeListener(focusListener);
		
		_editingEntry = (Entry)getIntent().getSerializableExtra("editingEntry");
		if(_editingEntry != null) {
			//we have an entry to edit
			updateLabelsForEdit();
		} else {
			_cloningEntry = (Entry)getIntent().getSerializableExtra("cloningEntry");
			if(_cloningEntry!=null) {
				updateLabelsForEdit();
			} else {
				String cheapDate = cal.get(Calendar.MONTH)+1 + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.YEAR);
				_dateTextfield.setText(cheapDate);
				hideCodeArea();
			}
		}
		
	}
	
	public void updateLabelsForEdit() {
		if(_editingEntry == null) {
			_editingEntry = _cloningEntry;
		}
		TextView header = (TextView) findViewById(R.id.newEntryHeaderText);
		if(_cloningEntry == null) {
			header.setText("Editing Entry");
		}
		int decimal;
		String hours;
		String[] timeParts = _editingEntry.getHours().split(Pattern.quote("."));
		try {
			hours = timeParts[0];
			decimal = Integer.parseInt(timeParts[1]);
		}catch(Exception e) {
			hours = "0";
			decimal = 0;
		}
		String actualMinutes = String.valueOf((int)(decimal * .6));
		for(int i = 0; i < hoursSpinner.getAdapter().getCount(); i++) {
			if(hoursSpinner.getAdapter().getItem(i).equals(String.valueOf(hours))) {
				hoursSpinner.setSelection(i);
			}
		}
		
		for(int i = 0; i < minutesSpinner.getAdapter().getCount(); i++) {
			if(minutesSpinner.getAdapter().getItem(i).equals(String.valueOf(actualMinutes))) {
				minutesSpinner.setSelection(i);
			}
		}
		
		String matterName = "Matter search";
		if(_editingEntry.getMatterId() != null && !_editingEntry.getMatterId().equals("") && !_editingEntry.getMatterId().equals("null") && !_editingEntry.getMatterId().equals("NULL")) {
			_matterDescText.setText(_editingEntry.getName());
			_matterText.setText(_editingEntry.getMatterId());
		}
		
		if(_editingEntry.getTask_code_id() != null && !_editingEntry.getTask_code_id().equals("") && !_editingEntry.getTask_code_id().equals("null") && !_editingEntry.getTask_code_id().equals("NULL")) {
			_taskText.setText(_editingEntry.getTask_code_id());
			_componentText.setText(_editingEntry.getComponent_code_id());
			showCodeArea();
			//get code descriptions
			operation = "get_code_descriptions";
			DoOperation o = new DoOperation();
			o.execute();
		} else {
			//check if there are codes for this matter
			operation = "check_code_groups";
			DoOperation o = new DoOperation();
			o.execute();
		}
		
		if(_editingEntry.getDate() != null && !_editingEntry.getDate().equals("") && !_editingEntry.getDate().equals("null") && !_editingEntry.getDate().equals("NULL")) {
			_dateTextfield.setText(_editingEntry.getDate());
		}
		
		if(_editingEntry.getDescription() != null && !_editingEntry.getDescription().equals("")) {
			_descriptionTextfield.setText(_editingEntry.getDescription());
		}
		if(_cloningEntry != null) {
			_editingEntry = null;
		}
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		String cheapDate = (monthOfYear + 1) + "/" + dayOfMonth + "/" + year;
		
		_dateTextfield.setText(cheapDate);
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent i) {
		if(resultCode == 3) {	//matter search
			try {
				Matter selectedMatter = (Matter)i.getSerializableExtra("matter_object");
				_selectedMatter = selectedMatter;
				TextView matterName = (TextView) findViewById(R.id.matterSearchText);
				matterName.setText(selectedMatter.getName());
				
				TextView matterId = (TextView) findViewById(R.id.matterSearchSubText);
				matterId.setText(selectedMatter.getMatterId());
				
				_taskText.setText("Task Code");
				_taskDescText.setText("Tap to select task code");
				
				_componentText.setText("Component Code");
				_compDescText.setText("Tap to select component code");
				
				GetMatterCodes gmc = new GetMatterCodes();
				gmc.execute();
			} catch(Exception e) {
				
			}
		} else if(resultCode == 1) {	//task search
			try{
				Matter codes = (Matter)i.getSerializableExtra("code_object");
				String codeId = codes.getMatterId();
				String codeName = codes.getName();
				
				TextView taskName = (TextView) findViewById(R.id.taskCodeName);
				taskName.setText(codeName);
				
				TextView taskId = (TextView) findViewById(R.id.taskCodeId);
				taskId.setText(codeId);
			}catch(Exception e) {
				
			}
		} else if(resultCode == 2) {	//component search
			try{
				Matter codes = (Matter)i.getSerializableExtra("code_object");
				String codeId = codes.getMatterId();
				String codeName = codes.getName();
				
				TextView codeNameTv = (TextView) findViewById(R.id.componentCodeName);
				codeNameTv.setText(codeName);
				
				TextView componentId = (TextView) findViewById(R.id.componentCodeId);
				componentId.setText(codeId);
			} catch(Exception e) {
				
			}
		} else {
			
		}
		
	}
	
	 private View.OnFocusChangeListener focusListener = new View.OnFocusChangeListener()
    {
        @Override
        public void onFocusChange(View v, boolean hasFocus)
        {        
        	EditText edit = (EditText) v;
            int len = edit.getText().toString().length();
            if (hasFocus)
            {
                _datePickerDialog.show();
 
            } else {
            	edit.setSelection(0, len);
            }
        }
    };
    
    public void buttonClicked(View v) {
        switch (v.getId()) {
          case R.id.entryCancel:
            //cancel button clicked
        	  super.onBackPressed();
        	  
        	  
            break;
          case R.id.entrySubmit:
            //submit pressed
        	  String matterId, date, hours, minutes, description, task, comp;
        	  boolean valid = true;
        	  boolean codeViewVisible = findViewById(R.id.codeView).getVisibility() == View.VISIBLE; 
        	  
        	  
        	  later = "inactive";
        	  if(_laterSwitch.isChecked()) {
        		  later = "active";
        	  }
        	  
        	  if(!codeViewVisible) {
        		  task = "";
        		  comp = "";
        	  } else {
        		  task = _taskText.getText().toString();
        		  comp = _componentText.getText().toString();
        	  }
        	  
        	  if(task.equals("Tap to select task code") || task.equals("Task Code")) {
        		  task = "";
        	  }
        	  
        	  if(comp.equals("Tap to select component code") || comp.equals("Component Code")) {
        		  comp = "";
        	  }
        	  
        	  matterId = _matterText.getText().toString();
        	  date = _dateTextfield.getText().toString();
        	  hours = hoursSpinner.getSelectedItem().toString();
        	  minutes = minutesSpinner.getSelectedItem().toString();
        	  description = _descriptionTextfield.getText().toString();
        	  if(matterId.equals("Tap to select matter")) {
        		  matterId = "";
        	  }
        	  Calendar cal = Calendar.getInstance();
        	  SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        	  format.setLenient(false);
	   	       try {
	   	            cal.setTime(format.parse(date));
	   	            valid = true;
	   	       }
	   	       catch(ParseException e){
	   	            valid = false;
	   	       }
	   	       
	   	       if(!valid) {
	   	    	SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
	        	  format2.setLenient(false);
		   	       try {
		   	    	cal.setTime(format2.parse(date));
		   	            valid = true;
		   	       }
		   	       catch(ParseException e){
		   	            valid = false;
		   	       }
	   	       }
	   	       Calendar current = Calendar.getInstance();
	   	       current.add(Calendar.MONTH, 1);
	   	       Calendar past = Calendar.getInstance();
	   	       past.add(Calendar.MONTH, -3);
	   	       boolean dateOutside = false;
	   	       if(current.before(cal) || past.after(cal)) {
	   	    	   dateOutside = true;
	   	       }
        	  
	   	       Builder a = new AlertDialog.Builder(NewEntryActivity.this)
	   	       							.setTitle("Incomplete Entry")
   	       								.setIcon(android.R.drawable.ic_dialog_alert)
   	       								.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

											@Override
											public void onClick(DialogInterface dialog,int which) {
												
												String matterId, date, hours, minutes, description, task, comp;
									        	  boolean valid = true;
									        	  boolean codeViewVisible = findViewById(R.id.codeView).getVisibility() == View.VISIBLE; 
									        	  
									        	  
									        	  later = "inactive";
									        	  if(_laterSwitch.isChecked()) {
									        		  later = "active";
									        	  }
									        	  
									        	  if(!codeViewVisible) {
									        		  task = "";
									        		  comp = "";
									        	  } else {
									        		  task = _taskText.getText().toString();
									        		  comp = _componentText.getText().toString();
									        	  }
									        	  
									        	  if(task.equals("Tap to select task code")) {
									        		  task = "";
									        	  }
									        	  
									        	  if(comp.equals("Tap to select component code")) {
									        		  comp = "";
									        	  }
									        	  
									        	  matterId = _matterText.getText().toString();
									        	  date = _dateTextfield.getText().toString();
									        	  hours = hoursSpinner.getSelectedItem().toString();
									        	  minutes = minutesSpinner.getSelectedItem().toString();
									        	  description = _descriptionTextfield.getText().toString();
									        	  if(matterId.equals("Tap to select matter")) {
									        		  matterId = "";
									        	  }
									        	  
									        	  SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
									        	  format.setLenient(false);
										   	       try {
										   	            format.parse(date);
										   	            valid = true;
										   	       }
										   	       catch(ParseException e){
										   	            valid = false;
										   	       }
										   	    shouldGoToIncomplete = true;
												if(_editingEntry == null) {
													operation = "submit_new";
													
											   	      e = new Entry();
								        			  e.setComponent_code_id(comp);
								        			  e.setDate(date);
								        			  e.setDate(e.getDBFormattedDate());
								        			  e.setDescription(description);
								        			  e.setHours(hours);
								        			  e.setMinutes(minutes);
								        			  e.setMatterId(matterId);
								        			  e.setTask_code_id(task);
								        			  SubmitEntry s = new SubmitEntry();
								        			  s.execute();
												} else {
													operation = "update";
													
								        			  e = new Entry();
								        			  e.setComponent_code_id(comp);
								        			  e.setDate(date);
								        			  e.setDate(e.getDBFormattedDate());
								        			  e.setDescription(description);
								        			  e.setHours(hours);
								        			  e.setMinutes(minutes);
								        			  e.setMatterId(matterId);
								        			  e.setTask_code_id(task);
								        			  e.setId(_editingEntry.getId());
								        			  e.setTe(_editingEntry.getTe());
								        			  SubmitEntry s = new SubmitEntry();
								        			  s.execute();
												}
											}
   	       									
   	       								}).setNegativeButton(android.R.string.no, null);
	   	       ObscuredSharedPreferences preferences = new ObscuredSharedPreferences(this, this.getSharedPreferences("Precision Preferences", Context.MODE_PRIVATE)); 
        	  if(matterId.length() == 0) {
					a.setMessage("Matter is not selected. Save this entry to incomplete list?").show();
        	  } else if(hours.equals("0") && minutes.equals("0") && !preferences.getBoolean("zeroTime", false)) {
					a.setMessage("Hours field is blank. Save this entry to incomplete list?").show();
        	  } else if(description.length() == 0) {
        		 a.setMessage("Description is blank. Save this entry to incomplete list?").show();
        	  } else if(!valid) {
        		  //invalid date format
        		 a.setMessage("Date is invalid. Save this entry to incomplete list?").show();
        	  } else if(codeViewVisible && ((task == null || task.equals("")) || (comp == null || comp.equals("")))) {
        		 a.setMessage("This matter requires one or more task/component codes. Save this entry to incomplete list?")
					.show();
        	  }
        	  else {
        		  if(dateOutside) {
        			  Builder b = new AlertDialog.Builder(NewEntryActivity.this)
							.setTitle("Incomplete Entry")
							.setMessage("The date entered is not within the valid date range (3 months ago, 1 month in the future). Please select a valid date.")
							.setIcon(android.R.drawable.ic_dialog_alert)
							.setNegativeButton((CharSequence)"Okay", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									dateInvalidSubmitAnyway = false;
								}
								
							});
							
        			  AlertDialog ad = b.show();
        			  ad.show();
        		  } else {
        			  if(_editingEntry == null) {	//new entry
	        			  e = new Entry();
	        			  e.setComponent_code_id(comp);
	        			  e.setDate(date);
	        			  e.setDate(e.getDBFormattedDate());
	        			  e.setDescription(description);
	        			  e.setHours(hours);
	        			  e.setMinutes(minutes);
	        			  e.setMatterId(matterId);
	        			  e.setTask_code_id(task);
	        			  operation = "submit_new";
	        			  SubmitEntry s = new SubmitEntry();
	        			  s.execute();
	        		  } else {
	        			  //editing entry (update)
	        			  operation = "update";
	        			  e = new Entry();
	        			  e.setComponent_code_id(comp);
	        			  e.setDate(date);
	        			  e.setDate(e.getDBFormattedDate());
	        			  e.setDescription(description);
	        			  e.setHours(hours);
	        			  e.setMinutes(minutes);
	        			  e.setMatterId(matterId);
	        			  e.setTask_code_id(task);
	        			  e.setId(_editingEntry.getId());
	        			  if(_editingEntry.getTe().equals("3")) {
	        				  e.setTe("2");
	        			  } else {
	        				  e.setTe(_editingEntry.getTe());
	        			  }
	        			  SubmitEntry s = new SubmitEntry();
	        			  s.execute();
	        		  }
        		  }
        	  }
        	  
            break;
	    case R.id.matterSearchFrame:
	        //matter search pressed
	    	Intent mIntent = new Intent(NewEntryActivity.this, MatterSearchActivity.class);
	    	startActivityForResult(mIntent, 0);
	    	
	        break;
	    case R.id.taskSearchFrame:
	        //task search pressed
	    	Intent taskIntent = new Intent(NewEntryActivity.this, TaskSearchActivity.class);
	    	if(_selectedMatter != null && _selectedMatter.getTaskGroup() != null) {
	    		taskIntent.putExtra("task_group", _selectedMatter.getTaskGroup());
	    	} else {
	    		taskIntent.putExtra("task_group", _editingEntry.getTaskGroup());
	    	}
	    	startActivityForResult(taskIntent, 1);
	    	
	        break;
	    case R.id.componentSearchFrame:
	        //component search pressed
	    	Intent compIntent = new Intent(NewEntryActivity.this, ComponentSearchActivity.class);
	    	if(_selectedMatter != null && _selectedMatter.getComponentGroup() != null) {
	    		compIntent.putExtra("component_group", _selectedMatter.getComponentGroup());
	    	} else {
	    		compIntent.putExtra("component_group", _editingEntry.getComponentGroup());
	    	}
	    	startActivityForResult(compIntent, 2);
	        break;
	      }
    }
    
    public void hideCodeArea() {
    	
    	View codeFrame = (View) findViewById(R.id.codeView);
    	codeFrame.setVisibility(View.GONE);
    	//View restOfView = findViewById(R.id.restOfView);
    	//restOfView.setBottom(codeViewTopY);
    }
    
    public void showCodeArea() {
    	View codeFrame = (View) findViewById(R.id.codeView);
    	codeFrame.setVisibility(View.VISIBLE);
    }

    private class GetMatterCodes extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(NewEntryActivity.this);
			progressDialog.setMessage("Retrieving associated codes...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			String matterId = _selectedMatter.getMatterId();

			rqts = new RequestToServer("matter_codes");
			rqts.setMatterIdPost(matterId);
			
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 1);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			if (sjta.getJsObjResponse() != null) {
				try {
					if(sjta.getJsObjResponse().has("session")) {
						Intent mIntent = new Intent(NewEntryActivity.this, LoginActivity.class);
						mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
						mIntent.putExtra("invalid_session", true);
						startActivity(mIntent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
						finish();
						return;
					} else if (sjta.getJsObjResponse().get("num_codes") != null) {

						int count = sjta.getJsObjResponse().getInt("num_codes");
						if(count == 1) {
							//show task groups
							showCodeArea();
						} else {
							//hide task groups
							hideCodeArea();
						}
					}
				} catch (JSONException e) {
					Toast.makeText(NewEntryActivity.this, "Task code retrieval error. Please contact an administrator if the problem persists.", Toast.LENGTH_SHORT)
							.show();
				}

			}

			super.onPostExecute(result);

		}
	}
    
    private class SubmitEntry extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(NewEntryActivity.this);
			progressDialog.setMessage("Saving entry...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			if(operation.equals("submit_new")) {
				rqts = new RequestToServer("new_entry");
				rqts.setEntryAndLater(e, later, shouldGoToIncomplete);
			} else if(operation.equals("update")) {
				rqts = new RequestToServer("update_entry");
				if(e.getTe().equals("3")) {
					e.setTe("2");
				}
				rqts.setUpdateEntry(e, later, shouldGoToIncomplete);
			}
			
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 1);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			if (sjta.getJsObjResponse() != null) {
				if(sjta.getJsObjResponse().has("session")) {
					Intent mIntent = new Intent(NewEntryActivity.this, LoginActivity.class);
					mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					mIntent.putExtra("invalid_session", true);
					startActivity(mIntent);
					overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
					finish();
					return;
				} else {
					JSONObject o= sjta.getJsObjResponse();
					if (o.has("success")) {
						Intent i = new Intent();
						i.putExtra("removeThis", e.getId());
						setResult(202, i);
						finish();
					} else if(o.has("error")) {
						setResult(-1);
						finish();
					}
				}
			}

			super.onPostExecute(result);

		}
	}
    
    private class DoOperation extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(NewEntryActivity.this);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			if(operation.equals("check_code_groups")) {
				rqts = new RequestToServer("check_code_groups");
				rqts.setMatterIdPost(_matterText.getText().toString());
			} else if(operation.equals("get_code_descriptions")) {
				rqts = new RequestToServer("get_code_descriptions");
				if(_editingEntry!=null) {
					rqts.setCodes(_editingEntry.getComponentGroup(), _editingEntry.getTaskGroup(), _editingEntry.getComponent_code_id(), _editingEntry.getTask_code_id());
				} else if(_cloningEntry != null) {
					rqts.setCodes(_cloningEntry.getTaskGroup(), _cloningEntry.getComponentGroup(), _cloningEntry.getComponent_code_id(), _cloningEntry.getTask_code_id());
				}
			} else if(operation.equals("refresh_session")) {
				rqts = new RequestToServer("refresh_session");
			}

			sjta = new SenderToJSONAPI(rqts, 1001, 1);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			if (sjta.getJsObjResponse() != null) {
				try {
					if(sjta.getJsObjResponse().has("session")) {
						Intent mIntent = new Intent(NewEntryActivity.this, LoginActivity.class);
						mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
						mIntent.putExtra("invalid_session", true);
						startActivity(mIntent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
						finish();
						return;
					} else if (operation.equals("check_code_groups") && sjta.getJsObjResponse().get("num_codes") != null) {

						int count = sjta.getJsObjResponse().getInt("num_codes");
						if(count == 1) {
							//show task groups
							showCodeArea();
						} else {
							//hide task groups
							hideCodeArea();
						}
					} else if(operation.equals("get_code_descriptions")) {
						JSONArray codes = sjta.getJsObjResponse().getJSONArray("codes");
						JSONObject task = codes.getJSONObject(0);
						JSONObject comp = codes.getJSONObject(1);
						_taskDescText.setText(task.getString("code_desc"));
						_compDescText.setText(comp.getString("code_desc"));
					}
				} catch (JSONException e) {
					//Toast.makeText(NewEntryActivity.this, "Task code retrieval error. Please contact an administrator if the problem persists.", Toast.LENGTH_SHORT)
						//	.show();
				}

			}

			super.onPostExecute(result);

		}
	}

	@Override
	public void onComplete(int hours, int minutes) {
		// TODO Auto-generated method stub
		this.hoursSpinner.setSelection(hours);
		this.minutesSpinner.setSelection(minutes);
		this.operation = "refresh_session";
		DoOperation d = new DoOperation();
		d.execute();
	}
    
}
