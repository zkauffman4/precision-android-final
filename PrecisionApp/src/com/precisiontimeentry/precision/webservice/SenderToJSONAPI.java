package com.precisiontimeentry.precision.webservice;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * @author abrysov
 *
 */
public class SenderToJSONAPI {
	
	private static final String TAG = "_ha_SenderToAPIClass";
	
	public static final int TYPE_JSON_OBJ = 1001;
	public static final int TYPE_JSON_MASS = 1002;
	private static String PHPSESSID;
	
	private AbstractHttpClient httpclient = null;
	private HttpParams sendParams = null;
	
	private HttpResponse httpResponse = null;
	private HttpGet httpget = null;
	private HttpPost httppost = null;
	
	private JSONObject jsResponse = null;
	private JSONArray jsaResponse = null;
	private String sServerResponse;
	/**
	 * 
	 */
	public SenderToJSONAPI(RequestToServer _rtsHaven, int _typeResp, int typeOfRequest) {
		httpclient = new DefaultHttpClient();
		sendParams = new BasicHttpParams();
		
		HttpConnectionParams.setConnectionTimeout(sendParams, 200000);
		HttpConnectionParams.setSoTimeout(sendParams, 20000);
		String url = _rtsHaven.getUrl();
		//it is a GET request
		if(typeOfRequest == 0) {
			httpget = new HttpGet(url);
			if(SenderToJSONAPI.PHPSESSID != null) {
				httpget.addHeader("Cookie", "PHPSESSID="+SenderToJSONAPI.PHPSESSID);
			}
		} else {	//POST request
			httppost = new HttpPost(url);
			if(SenderToJSONAPI.PHPSESSID != null) {
				httppost.addHeader("Cookie", "PHPSESSID="+SenderToJSONAPI.PHPSESSID);
			}
			try {
				ArrayList<NameValuePair> l = _rtsHaven.getParams();
				if(l != null) {
					httppost.setEntity(new UrlEncodedFormEntity(_rtsHaven.getParams()));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
			
		}
		
		try {
			
			if(typeOfRequest == 0) {
				httpResponse = httpclient.execute(httpget);
			} else {
				httpResponse = httpclient.execute(httppost);
			}
			
			CookieStore store = httpclient.getCookieStore();
			List<Cookie> cookies = store.getCookies();
			
			if (cookies.isEmpty()) {
			   Log.d(TAG,"no cookies received");
			} else {
			   for (int i = 0; i < cookies.size(); i++) {
			      if(cookies.get(i).getName().contentEquals("PHPSESSID")) {
			         PHPSESSID = cookies.get(i).getValue();
			      }
			   }
			}
			
			sServerResponse = EntityUtils.toString(httpResponse.getEntity());
			
			Log.i(TAG, "FROM: " + sServerResponse);	
			
			if (_typeResp == TYPE_JSON_OBJ) {
				
				try {
					jsResponse = new JSONObject(sServerResponse);
				} catch (JSONException e) {
					e.printStackTrace();
					Log.e(TAG, "json error: " + e.getMessage());
				}	
				
			}else if (_typeResp == TYPE_JSON_MASS){
				
				try {
					
					jsaResponse = new JSONArray(sServerResponse);
					
				} catch (JSONException e) {
					e.printStackTrace();
					Log.e(TAG, "json error: " + e.getMessage());
				}
				
			}

			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			Log.e(TAG, "ClientProtocolException error: " + e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
            jsaResponse = null;
			Log.e(TAG, "IOException error: " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
    public boolean isError(){
        return sServerResponse.contains("INVALID") && sServerResponse.contains("access_denied");
    }
	/**
	 * @return the jsResponse (JSON object)
	 */
	public JSONObject getJsObjResponse() {
		return jsResponse;
	}
	
	public JSONArray getJsMassResponse() {
		return jsaResponse;
	}
}
