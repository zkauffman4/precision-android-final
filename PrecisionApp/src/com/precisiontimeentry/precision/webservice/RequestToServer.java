/** 
 * Created by abrysov at Mar 10, 2012
 */
package com.precisiontimeentry.precision.webservice;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;

import com.precisiontimeentry.precision.model.Entry;

import android.util.Base64;

/**
 * @author abrysov
 * 
 */
public class RequestToServer {

	private final String sLoginURL = "https://precisiontimeentry.com/api/login.php";
	private final String sMatterSearchURL =
	 "https://precisiontimeentry.com/api/get_matters.php";
	 private final String sGetMatterCodes =
	 "https://precisiontimeentry.com/api/check_code_groups.php";
	 private final String sGetTasks = "https://precisiontimeentry.com/api/get_task_codes.php";
	 private final String sGetSubmittables =
	 "https://precisiontimeentry.com/api/get_entries.php?submittables=1";
	 private final String sGetIncompletes =
			 "https://precisiontimeentry.com/api/get_entries.php?incomplete=1";
	 private final String sLast7 =
	 "https://precisiontimeentry.com/api/get_history.php?time=week";
	 private final String sLast30 =
			 "https://precisiontimeentry.com/api/get_history.php?time=month";
	 private final String sMatterHistory =
			 "https://precisiontimeentry.com/api/get_unique_matters_history.php";
	 private final String sMatterHistoryList =
			 "https://precisiontimeentry.com/api/get_matter_history.php";
	 private final String sTimeStatsWeek =
			 "https://precisiontimeentry.com/api/get_time_stats.php?period=week";
	 private final String sTimeStatsMonth =
			 "https://precisiontimeentry.com/api/get_time_stats.php?period=month";
	 private final String sTimeStatsYear =
			 "https://precisiontimeentry.com/api/get_time_stats.php?period=year";
	 private final String sGetMatterEntries =
			 "https://precisiontimeentry.com/api/get_matter_entries.php";
	 private final String sGetMatterHours =
			 "https://precisiontimeentry.com/api/get_matter_hours.php";
	 private final String sGetGraphData =
			 "https://precisiontimeentry.com/api/get_graph_data.php";
	 private final String sUpdateEmail =
			 "https://precisiontimeentry.com/api/update_email.php";
	 private final String sGetEmails =
			 "https://precisiontimeentry.com/api/get_emails.php";
	private final String sNewEntry =
			"https://precisiontimeentry.com/api/new_entry.php";
	private final String sCreateEmailLogs =
			"https://precisiontimeentry.com/api/create_log_entries.php?emails=1";
	private final String sCreateCallLogs =
			"https://precisiontimeentry.com/api/create_log_entries.php?calls=1";
	private final String sDeleteEntry =
			"https://precisiontimeentry.com/api/delete.php";
	private final String sSubmitEntry =
			"https://precisiontimeentry.com/api/submit.php";
	private final String sCheckCodeGroups =
			"https://precisiontimeentry.com/api/check_code_groups.php";
	private final String sGetCodeDescriptions =
			"https://precisiontimeentry.com/api/get_code_descriptions.php";
	private final String sUpdateEntry =
			"https://precisiontimeentry.com/api/update_entry.php";
	private final String sResetPassword =
			"https://precisiontimeentry.com/api/reset_password.php";
	private final String sCreateAccount =
			"https://precisiontimeentry.com/api/create_account.php";
	private final String sGetLogEntries =
			"https://precisiontimeentry.com/api/get_entries.php?logs=1";
	private final String sLogout =
			"https://precisiontimeentry.com/api/logout.php";
	private final String sRefreshSession = 
			"https://precisiontimeentry.com/api/refresh_session.php";
	//url to request
	private String url = null;
	
	ArrayList<NameValuePair> postParameters;
	
	//enum for method type (0 = GET, 1 = POST)
	private int method = 0;
	
	//contains the string for the POST request body
	private String requestBody = null;

	/**
	 * Login API Constructor for a login request.
	 */
	// public RequestToServer(String gebied) {
	// String[] xyilo = gebied.split(" ");
	// requestBody = sCatlistURL;
	// requestBody += "?";
	// requestBody += "key=";
	// requestBody += KEY;
	// requestBody += "&";
	// requestBody += "gebied=";
	// requestBody += xyilo[0];
	// }

	public RequestToServer(String sLogin, String sPassword) {
		url = sLoginURL;
		//method = 0;
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("u", sLogin));
		postParameters.add(new BasicNameValuePair("p", sPassword));
		
	}
	
	public RequestToServer(String operation) {
		if(operation == "matter_search") {
			url = sMatterSearchURL;
			method = 0;
		} else if(operation.equals("matter_codes")) {
			url = sGetMatterCodes;
			method = 1;
		} else if(operation.equals("get_task_codes") || operation.equals("get_component_codes")) {
			url = sGetTasks;
			method = 0;
		} else if(operation.equals("get_submittables")) {
			url = sGetSubmittables;
			method = 0;
		} else if(operation.equals("get_incompletes")) {
			url = sGetIncompletes;
			method = 0;
		} else if(operation.equals("history_last_7")) {
			url = sLast7;
			method = 0;
		} else if(operation.equals("history_last_30")) {
			url = sLast30;
			method = 0;
		} else if(operation.equals("history_by_matter")) {
			url = sMatterHistory;
			method = 0;
		} else if(operation.equals("matter_history_list")) {
			url = sMatterHistoryList;
			method = 0;
		} else if(operation.equals("stats_this_week")) {
			url = sTimeStatsWeek;
			method = 0;
		} else if(operation.equals("stats_this_month")) {
			url = sTimeStatsMonth;
			method = 0;
		} else if(operation.equals("stats_this_year")) {
			url = sTimeStatsYear;
			method = 0;
		} else if(operation.equals("get_matter_entries")) {
			url = sGetMatterEntries;
			method = 0;
		} else if(operation.equals("get_matter_hours")) {
			url = sGetMatterHours;
			method = 0;
		} else if(operation.equals("get_graph_data")) {
			url = sGetGraphData;
			method = 0;
		} else if(operation.equals("update_email")) {
			url = sUpdateEmail;
			method = 1;
		} else if(operation.equals("get_emails")) {
			url = sGetEmails;
			method = 0;
		} else if(operation.equals("new_entry")) {
			url = sNewEntry;
			method = 1;
		} else if(operation.equals("create_call_logs")) {
			url = sCreateCallLogs;
			method = 1;
		} else if(operation.equals("create_email_logs")) {
			url = sCreateEmailLogs;
			method = 1;
		} else if(operation.equals("delete_entry")) {
			url = sDeleteEntry;
			method = 1;
		} else if(operation.equals("submit_entry")) {
			url = sSubmitEntry;
			method = 1;
		} else if(operation.equals("check_code_groups")) {
			url = sCheckCodeGroups;
			method = 1;
		} else if(operation.equals("get_code_descriptions")) {
			url = sGetCodeDescriptions;
			method = 1;
		} else if(operation.equals("update_entry")) {
			url = sUpdateEntry;
			method = 1;
		} else if(operation.equals("reset_password")) {
			url = sResetPassword;
			method = 1;
		} else if(operation.equals("create_account")) {
			url = sCreateAccount;
			method = 1;
		} else if(operation.equals("get_log_entries")) {
			url = sGetLogEntries;
			method = 0;
		} else if(operation.equals("logout")) {
			url = sLogout;
			method = 0;
		} else if(operation.equals("refresh_session")) {
			url = sRefreshSession;
			method = 0;
		}
			
	}
	
	public void setAccountCreation(String username, String email, String billingId, String password, String firmCode) {
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("username", username));
		postParameters.add(new BasicNameValuePair("email", email));
		postParameters.add(new BasicNameValuePair("password", password));
		postParameters.add(new BasicNameValuePair("billingId", billingId));
		postParameters.add(new BasicNameValuePair("regCode", firmCode));
	}
	
	public void setUsernameAndEmail(String username, String email) {
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("username", username));
		postParameters.add(new BasicNameValuePair("email", email));
	}
	
	public void setUpdateEntry(Entry e, String later, boolean incomplete) {
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("entry", e.getId()));
		postParameters.add(new BasicNameValuePair("matter", e.getMatterId()));
		postParameters.add(new BasicNameValuePair("date", e.getDate()));
		postParameters.add(new BasicNameValuePair("hours", e.getHours()));
		postParameters.add(new BasicNameValuePair("minutes", e.getMinutes()));
		postParameters.add(new BasicNameValuePair("description", e.getDescription()));
		postParameters.add(new BasicNameValuePair("lcode", e.getTask_code_id()));
		postParameters.add(new BasicNameValuePair("acode", e.getComponent_code_id()));
		postParameters.add(new BasicNameValuePair("later", later));
		postParameters.add(new BasicNameValuePair("te", e.getTe()));
		if(incomplete) {
			postParameters.add(new BasicNameValuePair("incomplete", "1"));
		}
	}
	
	public void setCodes(String compGroup, String taskGroup, String compId, String taskId) {
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("lcode_group", taskGroup));
		postParameters.add(new BasicNameValuePair("acode_group", compGroup));
		postParameters.add(new BasicNameValuePair("lcode_id", taskId));
		postParameters.add(new BasicNameValuePair("acode_id", compId));
	}
	
	public void setEntryIdListArraySpot(String id, String listEnum, String arraySpot) {
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("id", id));
		postParameters.add(new BasicNameValuePair("array_spot", arraySpot));
		postParameters.add(new BasicNameValuePair("list", listEnum));
	}
	
	public void setMatterIdPost(String matterId) {
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("matter_id", matterId));
	}
	
	public void setPostParam(String key, String val) {
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair(key, val));
	}
	
	public void setMatterIdGet(String matterId) {
		url += "?matter_id="+matterId;
	}
	
	public void setCodeGroup(String group, String type) {
		url+="?type="+type+"&code_group="+group;
	}
	
	public void setTimePeriod(String period) {
		url += "?period="+period;
	}
	
	public void setTimePeriodAndType(String period, String type) {
		url += "?period="+period+"&type="+type;
	}
	
	public void setPasswordAndLogin(String pass, String lastLogin) {
		url+= "?password="+pass+"&last_login="+Base64.encodeToString(lastLogin.getBytes(), Base64.URL_SAFE);
		url = url.substring(0, url.length()-3);
	}
	
	public void setEntryAndLater(Entry e, String later, boolean incomplete) {
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("matter", e.getMatterId()));
		postParameters.add(new BasicNameValuePair("date", e.getDate()));
		postParameters.add(new BasicNameValuePair("hours", e.getHours()));
		postParameters.add(new BasicNameValuePair("minutes", e.getMinutes()));
		postParameters.add(new BasicNameValuePair("description", e.getDescription()));
		postParameters.add(new BasicNameValuePair("lcode", e.getTask_code_id()));
		postParameters.add(new BasicNameValuePair("acode", e.getComponent_code_id()));
		postParameters.add(new BasicNameValuePair("later", later));
		if(incomplete) {
			postParameters.add(new BasicNameValuePair("incomplete", "1"));
		}
	}
	
	public void setLogsJSON(JSONArray submittable) {
		String s = submittable.toString();
		s = Base64.encodeToString(s.getBytes(), Base64.DEFAULT);
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("json", s));
	}

	/**
	 * Locate API Constructor for a GPS request.
	 */
	
	public String getUrl() {
		return url;
	}

	public String getREQUEST() {

		return requestBody;
	}
	
	public int getMethod() {
		return method;
	}
	
	public ArrayList<NameValuePair> getParams() {
		return postParameters;
	}

	
}
