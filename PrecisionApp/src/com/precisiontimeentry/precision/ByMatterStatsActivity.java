package com.precisiontimeentry.precision;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.precisiontimeentry.precision.model.LoginData;
import com.precisiontimeentry.precision.model.Matter;
import com.precisiontimeentry.precision.model.NameValuePairAdapter;
import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;

public class ByMatterStatsActivity extends Activity {

	private ProgressDialog progressDialog;
	private ArrayList<Matter> mattersArray;
	private ArrayList<Matter> filteredArray;
	private ListView resultsList;
	private int type;	//1 = hours, or 2 = entries
	private static String period;
	private ImageView backBtn;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_by_matter);
		resultsList = (ListView) findViewById(R.id.byMatterResults);
		mattersArray = new ArrayList<Matter>();
		filteredArray = new ArrayList<Matter>();
		Intent i = getIntent();
		type = i.getIntExtra("type", 1);
		period = i.getStringExtra("period");
		backBtn = (ImageView) findViewById(R.id.backBtnByMatter);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(-1);
				finish();
			}
			
		});
		
		
		MattersTask m = new MattersTask();
		m.execute();
		
	}
	
	private class MattersTask extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(ByMatterStatsActivity.this);
			progressDialog.setMessage("Retrieving data...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

		
			if(type == 1)
				rqts = new RequestToServer("get_matter_hours");
			else
				rqts = new RequestToServer("get_matter_entries");
			
			rqts.setTimePeriod(period);
			
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 0);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			if (sjta.getJsObjResponse() != null) {
				try {
					if(sjta.getJsObjResponse().has("session")) {
						Intent mIntent = new Intent(ByMatterStatsActivity.this, LoginActivity.class);
						mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
						mIntent.putExtra("invalid_session", true);
						startActivity(mIntent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
						finish();
						return;
					}else if (sjta.getJsObjResponse().get("matters") != null) {
						JSONArray matters = sjta.getJsObjResponse().getJSONArray("matters");
						for(int i = 0; i < matters.length(); i++) {
							JSONObject o = matters.getJSONObject(i);
							String matter_name = o.getString("matter_name");
							String total;
							if(type == 1) {
								total = "Hours: "+o.getString("total_hours"); 
							} else {
								total = "Entries: "+o.getString("total_entries"); 
							}
							
							Matter m = new Matter();
							m.setMatterId(total);
							m.setName(matter_name);
							
							mattersArray.add(m);
						}

					}
				} catch (JSONException e) {
					
				}

			}

			super.onPostExecute(result);

			resultsList.setAdapter(new NameValuePairAdapter(ByMatterStatsActivity.this, mattersArray));
		}
	}
	
}
