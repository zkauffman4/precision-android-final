package com.precisiontimeentry.precision;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.precisiontimeentry.precision.model.LoginData;
import com.precisiontimeentry.precision.model.Matter;
import com.precisiontimeentry.precision.model.NameValuePairAdapter;
import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;

public class MatterSearchActivity extends Activity {

	private ProgressDialog progressDialog;
	private ArrayList<Matter> mattersArray;
	private ArrayList<Matter> filteredArray;
	private ListView resultsList;
	private NewEntryActivity caller;
	private boolean isFiltered;
	private ImageView backBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		isFiltered = false;
		setContentView(R.layout.activity_matter_search);
		resultsList = (ListView) findViewById(R.id.matterResults);
		mattersArray = new ArrayList<Matter>();
		filteredArray = new ArrayList<Matter>();
		MattersTask m = new MattersTask();
		m.execute();
		
		backBtn = (ImageView) findViewById(R.id.backBtnMatterSearch);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(-1);
				finish();
			}
			
		});
		
		resultsList.setOnItemClickListener(new OnItemClickListener() {
	         @Override
	         public void onItemClick(AdapterView<?> a, View v, int position, long id) { 
	        	 Object o = resultsList.getItemAtPosition(position);
	        	 Matter fullObject = (Matter)o;
	        	 Intent i = new Intent(getApplicationContext(), NewEntryActivity.class);
	        	 i.putExtra("matter_object", fullObject);
	        	 setResult(3, i);
	        	 finish();
	         }

			
	        });
		
		SearchView searchBar = (SearchView) findViewById(R.id.matterSearchBar);
		searchBar.setIconifiedByDefault(false);
		searchBar.requestFocus();
		searchBar.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextChange(String arg0) {
				// TODO Auto-generated method stub
				filteredArray.clear();
				for(int i = 0; i < mattersArray.size(); i++) {
					Matter m = mattersArray.get(i);
					if(m.getName().toLowerCase().contains(arg0.toLowerCase()) || m.getMatterId().toLowerCase().contains(arg0.toLowerCase())) {
						filteredArray.add(m);
					}
				}
				
				resultsList.setAdapter(new NameValuePairAdapter(MatterSearchActivity.this, filteredArray));
				return false;
			}

			@Override
			public boolean onQueryTextSubmit(String arg0) {
				// TODO Auto-generated method stub
				filteredArray.clear();
				for(int i = 0; i < mattersArray.size(); i++) {
					Matter m = mattersArray.get(i);
					if(m.getName().contains(arg0) || m.getMatterId().contains(arg0)) {
						filteredArray.add(m);
					}
				}
				
				resultsList.setAdapter(new NameValuePairAdapter(MatterSearchActivity.this, filteredArray));
				
				return false;
			}
			
		});
		
	}
	
	private class MattersTask extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(MatterSearchActivity.this);
			progressDialog.setMessage("Retrieving matters...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

		

			rqts = new RequestToServer("matter_search");
			
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 1);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			if (sjta.getJsObjResponse() != null) {
				try {
					if(sjta.getJsObjResponse().has("session")) {
						Intent mIntent = new Intent(MatterSearchActivity.this, LoginActivity.class);
						mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
						mIntent.putExtra("invalid_session", true);
						startActivity(mIntent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
						finish();
						return;
					} else if (sjta.getJsObjResponse().get("matters") != null) {
						JSONArray matters = sjta.getJsObjResponse().getJSONArray("matters");
						for(int i = 0; i < matters.length(); i++) {
							JSONObject o = matters.getJSONObject(i);
							String matter_name = o.getString("matter_name");
							String matter_id = o.getString("matter_id");
							String task_group = o.getString("task_group");
							String component_group = o.getString("component_group");
							Matter m = new Matter();
							m.setMatterId(matter_id);
							m.setName(matter_name);
							m.setTaskGroup(task_group);
							m.setComponentGroup(component_group);
							mattersArray.add(m);
						}

					}
				} catch (JSONException e) {
					Toast.makeText(MatterSearchActivity.this, "Could not retrieve matters, please try again", Toast.LENGTH_SHORT)
							.show();
					Log.e("", "Matter search failed !");
				}

			}

			super.onPostExecute(result);

				resultsList.setAdapter(new NameValuePairAdapter(MatterSearchActivity.this, mattersArray));
		}
	}
	
}
