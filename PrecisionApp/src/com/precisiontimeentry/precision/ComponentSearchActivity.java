package com.precisiontimeentry.precision;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.precisiontimeentry.precision.model.LoginData;
import com.precisiontimeentry.precision.model.Matter;
import com.precisiontimeentry.precision.model.NameValuePairAdapter;
import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

public class ComponentSearchActivity extends Activity {

	private ProgressDialog progressDialog;
	private ArrayList<Matter> codesArray;
	private ArrayList<Matter> filteredArray;
	private ListView resultsList;
	private NewEntryActivity caller;
	private String _group;
	private ImageView backBtn;
	private TextView header;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_task_search);
		resultsList = (ListView) findViewById(R.id.taskResults);
		header = (TextView) findViewById(R.id.taskHeader);
		header.setText("Components");
		codesArray = new ArrayList<Matter>();
		filteredArray = new ArrayList<Matter>();
		Intent i = getIntent();
		_group = i.getStringExtra("component_group");
		TaskSearchTask t = new TaskSearchTask();
		t.execute();
		
		resultsList.setOnItemClickListener(new OnItemClickListener() {
	         @Override
	         public void onItemClick(AdapterView<?> a, View v, int position, long id) { 
	        	 Object o = resultsList.getItemAtPosition(position);
	        	 Matter fullObject = (Matter)o;
	        	 Intent i = new Intent(getApplicationContext(), NewEntryActivity.class);
	        	 i.putExtra("code_object", fullObject);
	        	 setResult(2, i);
	        	 finish();
	         }
	        });
		
		backBtn = (ImageView) findViewById(R.id.backBtnTasks);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(-1);
				finish();
			}
			
		});
		
		SearchView searchBar = (SearchView) findViewById(R.id.taskSearchBar);
		searchBar.setIconifiedByDefault(false);
		searchBar.requestFocus();
		searchBar.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextChange(String arg0) {
				// TODO Auto-generated method stub
				filteredArray.clear();
				for(int i = 0; i < codesArray.size(); i++) {
					Matter m = codesArray.get(i);
					arg0 = arg0.toLowerCase();
					if(m.getName().toLowerCase().contains(arg0) || m.getMatterId().toLowerCase().contains(arg0)) {
						filteredArray.add(m);
					}
				}
				
				resultsList.setAdapter(new NameValuePairAdapter(ComponentSearchActivity.this, filteredArray));
				return false;
			}

			@Override
			public boolean onQueryTextSubmit(String arg0) {
				// TODO Auto-generated method stub
				filteredArray.clear();
				for(int i = 0; i < codesArray.size(); i++) {
					Matter m = codesArray.get(i);
					arg0 = arg0.toLowerCase();
					if(m.getName().toLowerCase().contains(arg0) || m.getMatterId().toLowerCase().contains(arg0)) {
						filteredArray.add(m);
					}
				}
				
				resultsList.setAdapter(new NameValuePairAdapter(ComponentSearchActivity.this, filteredArray));
				
				return false;
			}
			
		});
		
	}
	
	private class TaskSearchTask extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(ComponentSearchActivity.this);
			progressDialog.setMessage("Retrieving codes...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

		

			rqts = new RequestToServer("get_component_codes");
			rqts.setCodeGroup(_group, "component");
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 1);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			if (sjta.getJsObjResponse() != null) {
				try {
					if(sjta.getJsObjResponse().has("session")) {
						Intent mIntent = new Intent(ComponentSearchActivity.this, LoginActivity.class);
						mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
						mIntent.putExtra("invalid_session", true);
						startActivity(mIntent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
						finish();
						return;
					}else if (sjta.getJsObjResponse().get("tasks") != null) {
						JSONArray codes = sjta.getJsObjResponse().getJSONArray("tasks");
						for(int i = 0; i < codes.length(); i++) {
							JSONObject o = codes.getJSONObject(i);
							String codeId = o.getString("code_id");
							String codeName = o.getString("code_desc");
							Matter m = new Matter();
							m.setMatterId(codeId);
							m.setName(codeName);
							codesArray.add(m);
						}

					}
				} catch (JSONException e) {
					Toast.makeText(ComponentSearchActivity.this, "Could not retrieve components, please try again", Toast.LENGTH_SHORT)
							.show();
					Log.e("", "Component search failed !");
				}

			}

			super.onPostExecute(result);

				resultsList.setAdapter(new NameValuePairAdapter(ComponentSearchActivity.this, codesArray));
		}
	}
	
}
