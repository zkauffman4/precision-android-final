package com.precisiontimeentry.precision;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.precisiontimeentry.precision.model.Call;
import com.precisiontimeentry.precision.model.Email;
import com.precisiontimeentry.precision.model.LoginData;
import com.precisiontimeentry.precision.model.ObscuredSharedPreferences;
import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends Activity implements OnClickListener {

	private TextView newEntryBtn, pendingEntryBtn, historyBtn, StatsBtn, settingBtn;
	private ProgressDialog progressDialog;
	private String l, en;
	private ArrayList<Email> inboxList;
	private ArrayList<Email> sentList;
	private ArrayList<Call> sentCalls, receivedCalls;
	private boolean hasCalls;
	private boolean hasEmails;
	private boolean checkEmails, checkCalls;
	private ImageView logout;
	private ObscuredSharedPreferences preferences;
	private ObscuredSharedPreferences.Editor editor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		intialization();
		preferences = new ObscuredSharedPreferences(this, this.getSharedPreferences("Precision Preferences", Context.MODE_PRIVATE)); 
		editor = preferences.edit();
		if(preferences.getString("u_n_new", "").toString().toLowerCase().contains("demouser")) {
			new AlertDialog.Builder(HomeActivity.this)
			.setTitle("Welcome to Precision")
			.setMessage("Thank you for trying Precision Time Entry. To learn more about Precision Time Entry and our team, please visit www.precisiontimeentry.com or feel free to email us at support@precisiontimeentry.com!")
			.setIcon(android.R.drawable.ic_dialog_info)
			 .setNegativeButton(android.R.string.ok, null).show();
		}
		if(!(preferences.getString("u_n_new", "").equals(preferences.getString("u_n", "")))) {
			editor.putString("e", "");
			editor.commit();
			new AlertDialog.Builder(HomeActivity.this)
			.setTitle("Settings Cleared!")
			.setMessage("You were not the last person to log in on this device. For security purposes, your exchange settings were cleared. Please re-enter your information in the settings tab.")
			.setIcon(android.R.drawable.ic_dialog_alert)
			 .setNegativeButton(android.R.string.ok, null).show();
		}
		editor.putString("u_n", preferences.getString("u_n_new", ""));
		editor.commit();
		hasCalls = hasEmails = checkEmails = checkCalls = false;
		logout = (ImageView) findViewById(R.id.logout);
		logout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				LogoutTask l = new LogoutTask();
				l.execute();
				
			}
			
		});
		
		
		checkEmails = preferences.getBoolean("enableEmails", false);
		checkCalls = preferences.getBoolean("enableLogs", false);
		l = preferences.getString("last_login", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		String formatted = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		if(checkCalls) {
			setCallDetails(l);
			if(sentCalls.size() > 0 || receivedCalls.size() > 0) {
				hasCalls = true;
			}
			
		}	
		if(checkEmails) {
			en = preferences.getString("e", "");
			if(en != null && en.length() > 0) {
				EmailTask g = new EmailTask();
				g.execute();
			}
		}
		
		if(checkCalls && !checkEmails && hasCalls) {
			new AlertDialog.Builder(HomeActivity.this)
			.setTitle("New calls")
			.setIcon(R.drawable.featuredimage)
			.setMessage("You have sent or received calls since last login, would you like to create entries?")
			.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

			    public void onClick(DialogInterface dialog, int whichButton) {
			        //yes clicked
			    	Intent i = new Intent(HomeActivity.this, CallEmailLogActivity.class);
			    	i.putExtra("inbox", inboxList);
			    	i.putExtra("sent", sentList);
			    	i.putExtra("sent_calls", sentCalls);
			    	i.putExtra("received_calls", receivedCalls);
			    	startActivityForResult(i, 0);
			    	
			    }})
			 .setNegativeButton(android.R.string.no, null).show();
		}
		

	}
	
	@Override
	public void onBackPressed() {
	    LogoutTask l = new LogoutTask();
	    l.execute();
	}

	private void intialization() {

		newEntryBtn = (TextView) findViewById(R.id.btn_new_entry);
		pendingEntryBtn = (TextView) findViewById(R.id.btn_pending_entries);
		historyBtn = (TextView) findViewById(R.id.btn_history);
		StatsBtn = (TextView) findViewById(R.id.btn_new_time_stats);
		settingBtn = (TextView) findViewById(R.id.btn_settings);

		newEntryBtn.setOnClickListener(this);
		pendingEntryBtn.setOnClickListener(this);
		historyBtn.setOnClickListener(this);
		StatsBtn.setOnClickListener(this);
		settingBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {

		Intent mIntent = null;
		switch (v.getId()) {
		case R.id.btn_new_entry:
			mIntent = new Intent(HomeActivity.this, NewEntryActivity.class);

			break;
		case R.id.btn_pending_entries:
			mIntent = new Intent(HomeActivity.this, PendingTabsActivity.class);

			break;
		case R.id.btn_history:
			mIntent = new Intent(HomeActivity.this, HistoryTabsActivity.class);

			break;
		case R.id.btn_new_time_stats:
			mIntent = new Intent(HomeActivity.this, StatsActivity.class);

			break;
		case R.id.btn_settings:
			mIntent = new Intent(HomeActivity.this, SettingActivity.class);

			break;

		default:
			break;
		}

		if (mIntent != null) {
			startActivity(mIntent);
			overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
		}
	}
	
	private class EmailTask extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(HomeActivity.this);
			progressDialog.setMessage("Retrieving calls and emails...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			rqts = new RequestToServer("get_emails");
			String formatted = l;
			formatted = formatted.substring(0, formatted.length()-4);
			rqts.setPasswordAndLogin(en, formatted);
			
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 0);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();
			inboxList = new ArrayList<Email>();
			sentList = new ArrayList<Email>();
			if (sjta.getJsObjResponse() != null) {
				try {
					if(sjta.getJsObjResponse().has("session")) {
						Intent mIntent = new Intent(HomeActivity.this, LoginActivity.class);
						mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
						mIntent.putExtra("invalid_session", true);
						startActivity(mIntent);
						overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
						finish();
						return;
					} else if(sjta.getJsObjResponse().has("error_code")) {
						int error = sjta.getJsObjResponse().getInt("error_code");
						if(error == 2 && hasCalls) {
							new AlertDialog.Builder(HomeActivity.this)
							.setTitle("New contact logs")
							.setIcon(R.drawable.featuredimage)
							.setMessage("There was an error connecting to the exchange server. Please check your credentials and try again. You have new "
									+ "calls since last login, would you like to create entries?")
							.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

							    public void onClick(DialogInterface dialog, int whichButton) {
							        //yes clicked
							    	Intent i = new Intent(HomeActivity.this, CallEmailLogActivity.class);
							    	i.putExtra("inbox", inboxList);
							    	i.putExtra("sent", sentList);
							    	i.putExtra("sent_calls", sentCalls);
							    	i.putExtra("received_calls", receivedCalls);
							    	startActivityForResult(i, 0);
							    	
							    }})
							 .setNegativeButton(android.R.string.no, null).show();
						} else if(error == 2) {
							new AlertDialog.Builder(HomeActivity.this)
							.setTitle("Email connection error")
							.setIcon(R.drawable.featuredimage)
							.setMessage("There was an error connecting to the exchange server. Please check your credentials and try again.")
							 .setNegativeButton(android.R.string.ok, null).show();
						}
					}
					else if (sjta.getJsObjResponse().get("mail") != null) {
						JSONObject boxes = sjta.getJsObjResponse().getJSONObject("mail");
						JSONArray inbox = boxes.getJSONArray("inbox");
						JSONArray sent = boxes.getJSONArray("sent");
						
						if(inbox.length() > 0 || sent.length() > 0) {
						
							for(int i = 0; i < inbox.length(); i++) {
								JSONObject item = inbox.getJSONObject(i);
								Email e = new Email();
								e.setSubject(item.getString("subject"));
								e.setEmail(item.getString("email"));
								String da = item.getString("DateTime");
								SimpleDateFormat f = new SimpleDateFormat("MMM-dd-yyyy HH:mm:ss zzz");
								SimpleDateFormat dest = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								Date d = new Date();
								try {
									d = f.parse(da);
								} catch (ParseException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								e.setDate(dest.format(d));
								inboxList.add(e);
							}
							
							for(int i = 0; i < sent.length(); i++) {
								JSONObject item = inbox.getJSONObject(i);
								Email e = new Email();
								e.setSubject(item.getString("subject"));
								e.setEmail(item.getString("displayName"));
								e.setDate(item.getString("DateTime"));
								sentList.add(e);
							}
							String message = "You have sent or received calls or emails since last login, would you like to create entries?";
							if(!hasCalls) {
								message = "You have sent or received emails since last login, would you like to create entries?";
							}
							new AlertDialog.Builder(HomeActivity.this)
							.setTitle("New contact logs")
							.setIcon(R.drawable.featuredimage)
							.setMessage(message)
							.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

							    public void onClick(DialogInterface dialog, int whichButton) {
							        //yes clicked
							    	Intent i = new Intent(HomeActivity.this, CallEmailLogActivity.class);
							    	i.putExtra("inbox", inboxList);
							    	i.putExtra("sent", sentList);
							    	i.putExtra("sent_calls", sentCalls);
							    	i.putExtra("received_calls", receivedCalls);
							    	startActivityForResult(i, 0);
							    	
							    }})
							 .setNegativeButton(android.R.string.no, null).show();
							
						} else if(hasCalls) {
							//have calls  but no emails
							new AlertDialog.Builder(HomeActivity.this)
							.setTitle("New calls")
							.setIcon(R.drawable.featuredimage)
							.setMessage("You have sent or received new calls since last login, would you like to create entries?")
							.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

							    public void onClick(DialogInterface dialog, int whichButton) {
							        //yes clicked
							    	Intent i = new Intent(HomeActivity.this, CallEmailLogActivity.class);
							    	i.putExtra("inbox", inboxList);
							    	i.putExtra("sent", sentList);
							    	i.putExtra("sent_calls", sentCalls);
							    	i.putExtra("received_calls", receivedCalls);
							    	startActivityForResult(i, 0);
							    	
							    }})
							 .setNegativeButton(android.R.string.no, null).show();
						}

					}
				} catch (JSONException e) {
					if(hasCalls) {
						//have calls  but no emails
						new AlertDialog.Builder(HomeActivity.this)
						.setTitle("New calls")
						.setIcon(R.drawable.featuredimage)
						.setMessage("You have sent or received new calls since last login, would you like to create entries?")
						.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

						    public void onClick(DialogInterface dialog, int whichButton) {
						        //yes clicked
						    	Intent i = new Intent(HomeActivity.this, CallEmailLogActivity.class);
						    	i.putExtra("inbox", inboxList);
						    	i.putExtra("sent", sentList);
						    	i.putExtra("sent_calls", sentCalls);
						    	i.putExtra("received_calls", receivedCalls);
						    	startActivityForResult(i, 0);
						    	
						    }})
						 .setNegativeButton(android.R.string.no, null).show();
					}
				}

			} else if(hasCalls) {
				//only have calls, no emails
				new AlertDialog.Builder(HomeActivity.this)
				.setTitle("New calls")
				.setIcon(R.drawable.featuredimage)
				.setMessage("You have sent or received new calls since last login, would you like to create entries?")
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

				    public void onClick(DialogInterface dialog, int whichButton) {
				        //yes clicked
				    	Intent i = new Intent(HomeActivity.this, CallEmailLogActivity.class);
				    	i.putExtra("inbox", inboxList);
				    	i.putExtra("sent", sentList);
				    	i.putExtra("sent_calls", sentCalls);
				    	i.putExtra("received_calls", receivedCalls);
				    	startActivityForResult(i, 0);
				    	
				    }})
				 .setNegativeButton(android.R.string.no, null).show();
			}

			super.onPostExecute(result);

		}
	}

	private void setCallDetails(String lastLogin) {

		if(sentCalls == null)
			sentCalls = new ArrayList<Call>();
		else
			sentCalls.clear();
		
		if(receivedCalls == null)
			receivedCalls = new ArrayList<Call>();
		else
			receivedCalls.clear();
		
		long millis = 0;
		try {
			SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");
			Date d = f.parse(lastLogin);
			millis = d.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			millis = (new Date()).getTime();
		}
		
        Cursor managedCursor = this.getContentResolver().query(CallLog.Calls.CONTENT_URI,
                null, CallLog.Calls.DATE + ">?", new String[] { String.valueOf(millis) }, CallLog.Calls.DATE + " DESC");
       

        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        
        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(number);
            String nameStr = managedCursor.getString(name);
            String callType = managedCursor.getString(type);
            long callDate = managedCursor.getLong(date);
            //Date callDayTime = new Date(Long.valueOf(callDate));
            int callDuration = managedCursor.getInt(duration);
            Call c = new Call();
            c.setDate(callDate);
            c.setLength(callDuration);
            if(nameStr != null) {
            	c.setPhoneNumber(nameStr);
            } else {
            	c.setPhoneNumber(phNumber);
            }
            
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
            case CallLog.Calls.OUTGOING_TYPE:
                sentCalls.add(c);
                break;

            case CallLog.Calls.INCOMING_TYPE:
                receivedCalls.add(c);
                break;

            }
            
        }
        managedCursor.close();
    }
	
	private class LogoutTask extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(HomeActivity.this);
			progressDialog.setMessage("Logging out...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			rqts = new RequestToServer("logout");
			
			//the "1" is to designate POST request
			sjta = new SenderToJSONAPI(rqts, 1001, 0);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			
			Intent mIntent = new Intent(HomeActivity.this, LoginActivity.class);
			mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(mIntent);
			overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);


			super.onPostExecute(result);

		}
	}
	
}
