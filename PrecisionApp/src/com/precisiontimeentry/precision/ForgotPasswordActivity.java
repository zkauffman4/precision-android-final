package com.precisiontimeentry.precision;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.precisiontimeentry.precision.webservice.RequestToServer;
import com.precisiontimeentry.precision.webservice.SenderToJSONAPI;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ForgotPasswordActivity extends Activity {
	
	private EditText _emailTextfield, _usernameTextfield;
	private Button _reset, _back;
	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private Matcher matcher;
	private Pattern pattern;
	private ProgressDialog progressDialog;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_forgot_password);
		
		_emailTextfield = (EditText) findViewById(R.id.txt_forgot_email);
		_usernameTextfield = (EditText) findViewById(R.id.txt_forgot_username);
		
		_reset = (Button) findViewById(R.id.btn_reset);
		_back = (Button) findViewById(R.id.btn_reset_to_login);
		
		_reset.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//check fields and attempt reset
				pattern = Pattern.compile(EMAIL_PATTERN);
				matcher = pattern.matcher(_emailTextfield.getText().toString());
				if(_usernameTextfield.getText().toString().length() == 0) {
					Toast.makeText(ForgotPasswordActivity.this, "Username cannot be empty!", Toast.LENGTH_SHORT).show();
				}else if(_emailTextfield.getText().toString().length() == 0) {
					Toast.makeText(ForgotPasswordActivity.this, "Email cannot be empty!", Toast.LENGTH_SHORT).show();
				}else if(!matcher.matches()) {
					Toast.makeText(ForgotPasswordActivity.this, "Email is invalid!", Toast.LENGTH_SHORT).show();
				} else {
					//try to server
					ForgotPassword f = new ForgotPassword();
					f.execute();
				}
			}
			
		});
		
		_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setResult(-1);
				finish();
			}
			
		});
		
	}
	
	private class ForgotPassword extends AsyncTask<Void, Void, Void> {

		RequestToServer rqts; 
		SenderToJSONAPI sjta;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(ForgotPasswordActivity.this);
			progressDialog.setMessage("Sending password reset email...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setCancelable(true);
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			rqts = new RequestToServer("reset_password");
			rqts.setUsernameAndEmail(_usernameTextfield.getText().toString(), _emailTextfield.getText().toString());

			sjta = new SenderToJSONAPI(rqts, 1001, 1);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progressDialog.dismiss();

			if (sjta.getJsObjResponse() != null) {
				try {
					int code = sjta.getJsObjResponse().getInt("response_code");
					switch(code) {
					case 1:
						//success
						setResult(101);
						finish();
						break;
					case 2:
						//invalid combo
						Toast.makeText(ForgotPasswordActivity.this, "Username and email combonation is invalid!", Toast.LENGTH_SHORT).show();
						break;
					case 3:
						//failed to update pw
						break;
					case 4:
						//failed to mail string
						break;
					}
				} catch (JSONException e) {
					
				}

			}

			super.onPostExecute(result);

		}
	}

}
