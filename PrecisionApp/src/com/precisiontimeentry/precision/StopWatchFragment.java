package com.precisiontimeentry.precision;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class StopWatchFragment extends DialogFragment {
	private Button doneBtn, clearBtn, startPauseBtn;
	private TextView timerLabel;
	private boolean running;
	private Timer timer;
	private TimerTask timerTask;
	private final Handler handler = new Handler();
	private int timerCount = 0;
	public StopWatchFragment() {}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    View view = inflater.inflate(R.layout.layout_stopwatch, container);
	    timerLabel = (TextView) view.findViewById(R.id.timerLabel);
	    doneBtn = (Button) view.findViewById(R.id.timerDoneBtn);
	    clearBtn = (Button) view.findViewById(R.id.clearBtn);
	    startPauseBtn = (Button) view.findViewById(R.id.startPauseBtn);
	    timerCount = 0;
	   	getDialog().setTitle("Timer");
	    return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
	    super.onActivityCreated(savedInstanceState);
	    doneBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				//later add shit
				int hours = timerCount/3600;
                double mins = (timerCount - (hours * 3600))/60.0;
                int minutes = 0;
                if(mins == 0) {
                	minutes = 0;
                } else if(mins < 9) {
                	minutes = 1;
                } else if(mins < 13.5) {
                	minutes = 2;
                } else if(mins < 16.5) {
                	minutes = 3;
                } else if(mins < 21) {
                	minutes = 4;
                } else if(mins < 27) {
                	minutes = 5;
                } else if(mins < 33) {
                	minutes = 6;
                } else if(mins < 39) {
                	minutes = 7;
                } else if(mins < 43.5) {
                	minutes = 8;
                } else if(mins < 46.5) {
                	minutes = 9;
                } else if(mins < 51) {
                	minutes = 10;
                } else {
                	minutes = 11;
                }
                StopWatchFragment.this.mListener.onComplete(hours, minutes);
				getActivity().getFragmentManager().beginTransaction().remove(StopWatchFragment.this).commit();
			}
	    	
	    });
	    
	    startPauseBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(running) {
					timer.cancel();
					startPauseBtn.setText("Start");
				} else {
					timer = new Timer();
					timerTask = new TimerTask() {
				        @Override
				        public void run() {
				            handler.post(new Runnable() {
				                @Override
				                public void run() {
				                    timerCount++;		//holds number of seconds
				                    int hours = timerCount/3600;
				                    int mins = (timerCount - (hours * 3600))/60;
				                    int seconds = timerCount%60;
				                    String s = String.format("%02d:%02d:%02d", hours, mins, seconds);
				                    timerLabel.setText(s);
				                }
				            });
				        }
				    };
					timer.schedule(timerTask, 1000, 1000);
					startPauseBtn.setText("Pause");
				}
				running = !running;
			}
	    	
	    });
	    clearBtn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				timerCount = 0;
				timerLabel.setText("00:00:00");
				running = false;
				if(timer != null) {
					timer.cancel();
				}
				startPauseBtn.setText("Start");
			}
	    	
	    });
	}
	public static interface OnCompleteListener {
	    public abstract void onComplete(int hours, int minutes);
	}

	private OnCompleteListener mListener;

	// make sure the Activity implemented it
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	    try {
	        this.mListener = (OnCompleteListener)activity;
	    }
	    catch (final ClassCastException e) {
	        throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
	    }
	}
}
